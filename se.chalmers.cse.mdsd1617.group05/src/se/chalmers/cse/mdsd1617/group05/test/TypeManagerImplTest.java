package se.chalmers.cse.mdsd1617.group05.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group05.impl.RoomManagerImpl;
import se.chalmers.cse.mdsd1617.group05.impl.TypeManagerImpl;

public class TypeManagerImplTest {
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}



	@Test
	public void testAddRoomType() {
		TypeManagerImpl tm = TypeManagerImpl.getInstance();
		//TypeManagerImpl.getInstance().addRoomType("bbb",155, 1);
		//fail("Not yet implemented");
		
		assertTrue(tm.addRoomType("aaa",198, 2));
		assertFalse(tm.addRoomType("aaa",155, 1));
		assertTrue(tm.fetchTypeByName("bbb")==null);
		assertTrue(tm.fetchTypeByName("aaa")!=null);
		
		tm.clearTypes();
	}

	@Test
	public void testUpdateRoomType() {
		TypeManagerImpl tm = TypeManagerImpl.getInstance();
		
		tm.addRoomType("aaa",198, 2);
		tm.addRoomType("bbb",198, 2);
		
		assertTrue(tm.updateRoomType("aaa", "ccc", 188, 2));
		assertFalse(tm.updateRoomType("bbb", "ccc", 188, 2));
		assertFalse(tm.updateRoomType("ddd", "eee", 188, 2));
		assertTrue(tm.fetchTypeByName("aaa")==null);
		assertTrue(tm.fetchTypeByName("ccc")!=null);
		
		tm.clearTypes();
		
	}

	@Test
	public void testRemoveRoomType() {
		TypeManagerImpl tm = TypeManagerImpl.getInstance();
		RoomManagerImpl rm = RoomManagerImpl.getInstance();
		
		tm.addRoomType("aaa",198, 2);
		tm.addRoomType("bbb",155, 1);
		rm.addRoom(1,"aaa");
		
		
		assertTrue(tm.fetchTypeByName("bbb")!=null);
		assertTrue(tm.removeRoomType("bbb"));
		assertFalse(tm.removeRoomType("aaa"));
		assertFalse(tm.removeRoomType("ccc"));
		assertTrue(tm.fetchTypeByName("bbb")==null);
		assertTrue(tm.fetchTypeByName("aaa")!=null);
		
		tm.clearTypes();
		
	}
	
	


}
