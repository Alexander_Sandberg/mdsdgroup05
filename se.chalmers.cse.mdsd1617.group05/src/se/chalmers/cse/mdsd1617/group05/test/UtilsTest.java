package se.chalmers.cse.mdsd1617.group05.test;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group05.util.Utils;

/*
 * @author Joakim
 */
public class UtilsTest {

	protected Date date;
	protected Date startDate;
	protected Date endDate;
	protected Date falseDate;
	
	protected String startDateString;
	protected String endDateString;
	protected String falseStartDateString;
	
	protected Calendar cal;
	@Before
	public void setUp() throws Exception {
		cal = Calendar.getInstance();
		cal.set(2016, 00, 01,00,00,00);
		startDate = cal.getTime();
		cal.set(2017,00,01,00,00,00);
		endDate = cal.getTime();
		cal.set(2016,11,16,00,00,00);
		date = cal.getTime();
		cal.set(2015,11,16,00,00,00);
		falseDate = cal.getTime();
		
		startDateString = "20160101";
		falseStartDateString = "20160101";
		endDateString = "20170101";
	}

	@After
	public void tearDown() throws Exception {
		
	}

	@Test
	public void testGetStringDate(){
		assertEquals(startDateString,Utils.getStringDate(startDate));
	}
	
	@Test
	public void testValidateStringDates(){
		assertFalse(Utils.validateStringDates(falseStartDateString, startDateString));
		assertTrue(Utils.validateStringDates(startDateString, endDateString));
	}
	
	@Test
	public void testIsDateIncluded(){
		assertFalse(Utils.isDateIncluded(falseDate, startDate, endDate));
		assertTrue(Utils.isDateIncluded(date, startDate, endDate));
	}
	
	
	public void isDateIncludedStringInput() {
		assertFalse(Utils.isDateIncluded(falseDate, startDateString, endDateString));
		assertTrue(Utils.isDateIncluded(date, startDateString, endDateString));
	}
	
	@Test
	public void testGetDateFromString(){
		int comparison = startDate.compareTo(Utils.getDateFromString(startDateString));
		assertEquals(1,comparison);
	}

}
