package se.chalmers.cse.mdsd1617.group05.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import org.eclipse.emf.common.util.EList;

import se.chalmers.cse.mdsd1617.group05.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group05.RoomReservation;
import se.chalmers.cse.mdsd1617.group05.BookingManager;
import se.chalmers.cse.mdsd1617.group05.Group05Factory;
import se.chalmers.cse.mdsd1617.group05.RoomManager;
import se.chalmers.cse.mdsd1617.group05.TypeManager;
import se.chalmers.cse.mdsd1617.group05.impl.Group05FactoryImpl;
import se.chalmers.cse.mdsd1617.group05.util.Utils;

/* @doc		This JUnit Test Case includes JUnit Tests for the functions
 * 			required to complete the following Use Cases:
 * 			- U.C. 2.1.1- Make a Booking
 * 			- U.C. 2.1.2- Search for Free Rooms
 * 			- U.C. 2.1.13 - Add Extra Cost To Rooms
 * @author 	Marco Trifance 
 * */
public class BMTest {

	protected Group05Factory factory;
	protected BookingManager bm;
	protected TypeManager tm;
	protected RoomManager rm;
	
	// Bookings ID references
	protected int rossID;
	protected int petrosyanID;
	protected int schillingID;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {}

	@Before
	public void setUp() throws Exception {
		factory = Group05FactoryImpl.init();
		bm = factory.createBookingManager();
		tm = factory.createTypeManager();
		rm = factory.createRoomManager();
		
		// Initialize Types
		tm.addRoomType("Single", 20.00, 1);
		tm.addRoomType("Double", 35.00, 2);
		tm.addRoomType("Triple", 40.00, 3);
		tm.addRoomType("Suite", 100.00, 4);
		
		// Initialize Rooms
		for(int i = 1; i < 6; i++){			// Rooms 1-5 are of type "Single"
			rm.addRoom(i, "Single");
		}
		for(int i = 6; i < 10; i++){		// Rooms 6-9 are of type "Double"
			rm.addRoom(i, "Double");
		}
		for(int i = 10; i < 13; i++){		// Rooms 10-12 are of type "Triple"
			rm.addRoom(i, "Triple");
		}
		rm.addRoom(13, "Suite");			// Room 13 is of type "Suite" (4 beds)
		
	}
	
	/* @doc Creates bookings in the BookingManager.
	 * 		Used to test functions:
	 * 		- getOverlappingBookings();
	 * 		- getFreeRooms();
	 * */
	private void initializeBookings(){
		// Initialize bookings
		rossID = bm.initiateBooking("Kevin", "20170201", "20170221", "Ross");
		bm.addRoomToBooking("Double", rossID);
		bm.addRoomToBooking("Double", rossID);
		bm.addRoomToBooking("Double", rossID);
		bm.addRoomToBooking("Double", rossID);
		bm.confirmBooking(rossID);
		petrosyanID = bm.initiateBooking("Giorgio", "20170207", "20170214", "Petrosyan");
		bm.addRoomToBooking("Suite", petrosyanID);
		bm.confirmBooking(petrosyanID);
		int schillingID = bm.initiateBooking("Joe", "20170215", "20170225", "Schilling");
		bm.addRoomToBooking("Single", schillingID);
		bm.addRoomToBooking("Triple", schillingID);
		bm.confirmBooking(schillingID);
	}
	

	@After
	public void tearDown() throws Exception {}

	/* @author  Marco Trifance
	 * Class: 	Utils.java
	 * */
	@Test
	public void testValidateName(){
		assertTrue(Utils.validateName("Mario", "Rossi"));
		assertFalse(Utils.validateName("Mario", ""));
		assertFalse(Utils.validateName("", "Rossi"));
	}
	
	/* @author  Marco Trifance
	 * Class: 	Utils.java
	 * */
	@Test
	public void testValidateStringDates(){
		assertTrue(Utils.validateStringDates("20161211", "20161212"));
		assertFalse(Utils.validateStringDates("20161211", "20111212"));
		assertFalse(Utils.validateStringDates("20161211", ""));
		assertFalse(Utils.validateStringDates("", "20161212"));
		assertFalse(Utils.validateStringDates("", ""));
		assertFalse(Utils.validateStringDates("123","456"));		
	}
	
	/* @author  Marco Trifance
	 * Class: 	Utils.java
	 * */
	@Test
	public void testIsDate(){
		assertTrue(Utils.isDate("20161111"));
		assertFalse(Utils.isDate("20165544"));
		assertFalse(Utils.isDate(""));
		assertFalse(Utils.isDate("aaa"));
	}
	
	/* Tested Function: getFreeRooms()
	 * Class: BookingManagerImpl.java
	 * Scenario: There is no existing booking stored in the Booking Manager.
	 * 			 (i.e. no overlapping bookings).
	 * */
	@Test
	public void testGetFreeRooms_noBookings() {
		assertEquals(0, bm.getFreeRooms(1, "20161212", "20160101").size());		// Invalid dates
		assertEquals(4, bm.getFreeRooms(1, "20161212", "20161213").size());		// 4 types available with at least 1 bed
		assertEquals(3, bm.getFreeRooms(2, "20161212", "20161213").size());		// 3 types available with at least 2 beds
		assertEquals(2, bm.getFreeRooms(3, "20161212", "20161213").size());		// 2 types available with at least 3 beds
		EList<FreeRoomTypesDTO> freeRooms = bm.getFreeRooms(4, "20161212", "20161213");
		assertEquals(1, freeRooms.size());
		assertEquals("Suite", freeRooms.get(0).getRoomTypeDescription());			// Available type description is "Suite"
		assertEquals(4, freeRooms.get(0).getNumBeds());								// Available type has 4 beds
		assertEquals(100.00, freeRooms.get(0).getPricePerNight(), 0.00);			// Available type costs 100.00$ per night
		assertEquals(0, bm.getFreeRooms(5, "20161212", "20161213").size());			// 0 type available with at least 5 beds
	}
	
	/* Tested Function: getFreeRooms()
	 * Class: BookingManagerImpl.java
	 * Scenario: Existing bookings in Booking Manager that overlap 
	 * 			with requested dates. 
	 * 			- 1 Suite booked within dates 2017-02-07 to 2017-02-14
	 * 			- 4 
	 * */
	@Test
	public void testGetFreeRooms_existingBookings() {
		EList<FreeRoomTypesDTO> freeRooms0 = bm.getFreeRooms(4, "20170207", "20170214");
		assertEquals(1, freeRooms0.size());
		assertEquals("Suite", freeRooms0.get(0).getRoomTypeDescription());
		assertEquals(1, freeRooms0.get(0).getNumFreeRooms());
		
		initializeBookings();
		EList<FreeRoomTypesDTO> freeRooms1 = bm.getFreeRooms(4, "20170210", "20170216");
		for(FreeRoomTypesDTO f:freeRooms1){
			System.out.println("TYPE :" + f.getRoomTypeDescription() + " | FREE ROOMS : " + f.getNumFreeRooms());
		}
		assertEquals(1, freeRooms1.size());
		assertEquals("Suite", freeRooms1.get(0).getRoomTypeDescription());
		assertEquals(0, freeRooms1.get(0).getNumFreeRooms());
				
		EList<FreeRoomTypesDTO> freeRooms2 = bm.getFreeRooms(4, "20170205", "20170210"); 
		assertEquals(1, freeRooms2.size());
		assertEquals("Suite", freeRooms2.get(0).getRoomTypeDescription());
		assertEquals(0, freeRooms2.get(0).getNumFreeRooms());
		
		EList<FreeRoomTypesDTO> freeRooms3 = bm.getFreeRooms(4, "20170205", "20170207");
		assertEquals(1, freeRooms3.size());
		assertEquals("Suite", freeRooms3.get(0).getRoomTypeDescription());
		assertEquals(0, freeRooms3.get(0).getNumFreeRooms());
		
		EList<FreeRoomTypesDTO> freeRooms4 = bm.getFreeRooms(4, "20170214", "20170215"); 
		assertEquals(1, freeRooms4.size());
		assertEquals("Suite", freeRooms4.get(0).getRoomTypeDescription());
		assertEquals(0, freeRooms4.get(0).getNumFreeRooms());
		
		EList<FreeRoomTypesDTO> freeRooms5 = bm.getFreeRooms(4, "20170201", "20170228"); 
		assertEquals(1, freeRooms5.size());
		assertEquals("Suite", freeRooms5.get(0).getRoomTypeDescription());
		assertEquals(0, freeRooms5.get(0).getNumFreeRooms());	
		
		EList<FreeRoomTypesDTO> freeRooms6 = bm.getFreeRooms(4, "20170205", "20170206");
		assertEquals(1, freeRooms6.size());
		assertEquals("Suite", freeRooms6.get(0).getRoomTypeDescription());
		assertEquals(1, freeRooms6.get(0).getNumFreeRooms()); 	
		
		EList<FreeRoomTypesDTO> freeRooms7 = bm.getFreeRooms(4, "20170215", "20170216");
		assertEquals(1, freeRooms7.size());
		assertEquals("Suite", freeRooms7.get(0).getRoomTypeDescription());
		assertEquals(1, freeRooms7.get(0).getNumFreeRooms());	
	}
	
	/* Tested Function: initiateBooking()
	 * Class: BookingManagerImpl.java
	 * Scenario: - Precondition: BM is empty (no bookings recorded)
	 * 			 - create one booking with valid data
	 * 			 - attempt to create one booking with invalid data
	 * 			 - create one booking with valid data
	 * 			 - Post-Condition: 2 bookings created with unique IDs
	 * */
	@Test
	public void testInitBooking(){
		assertEquals(0, bm.getBooking().size());
		int b1Id = bm.initiateBooking("Mario", "20161212", "20161213", "Rossi");
		assertNotEquals(-1, b1Id);
		assertEquals(1, bm.getBooking().size());
		assertFalse(bm.fetchBooking(b1Id).isConfirmed());			// booking is not confirmed
		int b2Id = bm.initiateBooking("John", "20161213", "20161211", "Doe");
		assertEquals(-1, b2Id);
		assertEquals(1, bm.getBooking().size());
		int b3Id = bm.initiateBooking("Per", "20150101", "20160101", "Persson");
		assertNotEquals(-1, b3Id);
		assertEquals(2, bm.getBooking().size());
		assertFalse(bm.fetchBooking(b3Id).isConfirmed());		
		assertTrue(b1Id != b3Id);
		
	}
	
	/* Tested Function: addRoomToBooking()
	 * Class: BookingManagerImpl.java
	 * Scenario: ....
	 * */
	@Test
	public void testAddRoomToBooking() {
		int b1Id = bm.initiateBooking("Mario", "20161212", "20161213", "Rossi");
		// Add 1 Single Room to booking
		assertTrue(bm.addRoomToBooking("Single", b1Id));
		assertEquals(1, bm.fetchBooking(b1Id).getRoomreservation().size());			
		RoomReservation rr1 = bm.fetchBooking(b1Id).getRoomreservation().get(0);
		assertEquals("Single", rr1.getType().getName());
		// Add 1 Single Room to booking
		assertTrue(bm.addRoomToBooking("Single", b1Id));
		assertEquals(2, bm.fetchBooking(b1Id).getRoomreservation().size());
		RoomReservation rr2 = bm.fetchBooking(b1Id).getRoomreservation().get(1);
		assertEquals("Single", rr2.getType().getName());
		// Attempt to add 1 Single Room to booking (wrong booking id)
		assertFalse(bm.addRoomToBooking("Double", 12));				
		assertEquals(2, bm.fetchBooking(b1Id).getRoomreservation().size());
		// Add one Double room to booking
		assertTrue(bm.addRoomToBooking("Double", b1Id));
		assertEquals(3, bm.fetchBooking(b1Id).getRoomreservation().size());
		RoomReservation rr3 = bm.fetchBooking(b1Id).getRoomreservation().get(2);
		assertEquals("Double", rr3.getType().getName());
		// Attempt to add room reservation with wrong type
		assertFalse(bm.addRoomToBooking("NON_EXISTING", b1Id));
		// Confirm booking
		bm.confirmBooking(b1Id);
		// Attempt to add room reservation to confirmed booking
		assertFalse(bm.addRoomToBooking("Triple", b1Id));
	}
	
	@Test
	public void testAddRoomToBooking_overbooking() {
		int b1Id = bm.initiateBooking("Mario", "20161212", "20161213", "Rossi");
		assertTrue(bm.addRoomToBooking("Suite", b1Id));
		assertFalse(bm.addRoomToBooking("Suite", b1Id));		// Only 1 Suite in the Hotel
		assertTrue(bm.addRoomToBooking("Triple", b1Id));
		assertTrue(bm.addRoomToBooking("Triple", b1Id));
		assertTrue(bm.addRoomToBooking("Triple", b1Id));
		assertFalse(bm.addRoomToBooking("Triple", b1Id));		// Only 3 Triple rooms in the Hotel
		assertTrue(bm.addRoomToBooking("Single", b1Id));
		assertTrue(bm.addRoomToBooking("Single", b1Id));
		assertTrue(bm.addRoomToBooking("Single", b1Id));
		assertTrue(bm.addRoomToBooking("Single", b1Id));
		assertTrue(bm.addRoomToBooking("Single", b1Id));
		assertFalse(bm.addRoomToBooking("Single", b1Id));		// Only 5 single rooms in the Hotel
		
	}
	
	/* Tested Function: getOverlappingBookings()
	 * Class: BookingManagerImpl.java
	 * */
	@Test
	public void testGetOverlappingBookings(){
		initializeBookings();
		assertEquals(3, bm.getOverlappingBookings("20170201", "20170228").size());
		assertEquals(1, bm.getOverlappingBookings("20170201", "20170206").size());
		assertEquals(2, bm.getOverlappingBookings("20170208", "20170209").size());
		assertEquals(3, bm.getOverlappingBookings("20170207", "20170215").size());
		assertEquals(2, bm.getOverlappingBookings("20170215", "20170228").size());
		assertEquals(1, bm.getOverlappingBookings("20170222", "20170225").size());
	}
	
	/* Tested Function: getUnblockedRooms()
	 * Class: RoomManagerImpl.java
	 * */
	@Test
	public void testGetUnblockedRooms(){
		// Step 1: Empty EList returned (no room has 6 beds)
		assertEquals(0, rm.getUnblockedRooms(6).size());
		
		// Step 2: Two Types have unblocked rooms with at least 3 beds 
		assertEquals(2, rm.getUnblockedRooms(3).size());
		assertEquals("Triple", rm.getUnblockedRooms(3).get(0).getRoomTypeDescription());
		assertEquals(3, rm.getUnblockedRooms(3).get(0).getNumFreeRooms());	// 3 "unblocked Triple" rooms
		assertEquals("Suite", rm.getUnblockedRooms(3).get(1).getRoomTypeDescription());
		assertEquals(1, rm.getUnblockedRooms(3).get(1).getNumFreeRooms());	// 1 "unblocked Suite"
		
		// Step 3: All 4 types have unblocked rooms with at least 1 bed
		assertEquals(4, rm.getUnblockedRooms(1).size());	
		
		// Step 4: Required min 4 beds
		EList<FreeRoomTypesDTO> ub0 = rm.getUnblockedRooms(4);			
		// One type has unblocked rooms with at least 4 beds
		assertEquals(1, ub0.size());			
		// Type description is "Suite"
		assertEquals("Suite", ub0.get(0).getRoomTypeDescription());
		// Num of beds is 4
		assertEquals(4, ub0.get(0).getNumBeds());						
		// Available count is 1
		assertEquals(1, ub0.get(0).getNumFreeRooms());		// 1 "unblocked Suite"
		
		// Step 5. Block "Suite"
		rm.blockRoom(13);												
		// Now 0 types have unblocked rooms with at least 4 beds
		assertEquals(0, rm.getUnblockedRooms(4).size());	
		
		// Step 6. Required min 3 beds
		EList<FreeRoomTypesDTO> ub1 = rm.getUnblockedRooms(3);
		// Now 1 type has unblocked rooms with at least 3 beds (Suite is blocked...)
		assertEquals(1, ub1.size());	
		// Type description is "Triple"
		assertEquals("Triple", ub1.get(0).getRoomTypeDescription());
		// Num of beds is 3
		assertEquals(3, ub1.get(0).getNumBeds());						
		// Available count is 3
		assertEquals(3, ub1.get(0).getNumFreeRooms());
		
		// Step 7. Block "Triple" rooms #11 and 12 
		rm.blockRoom(11);
		rm.blockRoom(12);
		EList<FreeRoomTypesDTO> ub2 = rm.getUnblockedRooms(3);
		// Still 1 type has unblocked rooms with at least 3 beds (room #10 available)
		assertEquals(1, ub2.size());	
		// Type description is "Triple"
		assertEquals("Triple", ub2.get(0).getRoomTypeDescription());
		// Available count is 1
		assertEquals(1, ub2.get(0).getNumFreeRooms());
	}
	
	/* Tested Function: addCostToToom()
	 * Class:			BookingManagerImpl.java
	 * */
	@Test 
	public void testAddCostToRoom(){
		// 1. Wrong booking ID (non-existing bookings)
		assertFalse(bm.addCostToRoom(1, rossID, "laundry", 5.00));
		
		// 2. Initialize bookings and checkIn booking with id 'rossID'
		initializeBookings();
		assertTrue(bm.checkInBooking(rossID));
		// Room one is not included in booking (room 1 is Single, Mr. Ross booked 4 doubles)
		assertFalse(bm.addCostToRoom(1, rossID, "laundry", 5.00));
		// Correct room
		assertTrue(bm.addCostToRoom(6, rossID, "laundry", 5.00));
		// Booking with id 'petrosyanID' booking is not checked in yet (no Room assigned yet)
		assertFalse(bm.addCostToRoom(13, petrosyanID, "laundry", 5.00));
		
		// 3. Check-in petrosyanID
		assertTrue(bm.checkInBooking(petrosyanID));
		assertTrue(bm.addCostToRoom(13, petrosyanID, "bar", 20.00));
	}
}
