package se.chalmers.cse.mdsd1617.group05.test;

import static org.junit.Assert.*;

import java.util.Date;

import org.eclipse.emf.common.util.BasicEList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group05.Booking;
import se.chalmers.cse.mdsd1617.group05.BookingManager;
import se.chalmers.cse.mdsd1617.group05.Group05Factory;
import se.chalmers.cse.mdsd1617.group05.Room;
import se.chalmers.cse.mdsd1617.group05.RoomManager;
import se.chalmers.cse.mdsd1617.group05.RoomReservation;
import se.chalmers.cse.mdsd1617.group05.Status;
import se.chalmers.cse.mdsd1617.group05.Type;
import se.chalmers.cse.mdsd1617.group05.TypeManager;
import se.chalmers.cse.mdsd1617.group05.impl.Group05FactoryImpl;
import se.chalmers.cse.mdsd1617.group05.util.Utils;

public class BookingManagerImplTest {
	
	protected Group05Factory factory;
	protected BookingManager bm;
	protected TypeManager tm;
	protected RoomManager rm;
	protected Booking b1;
	protected Booking b2;
	protected Booking b3;
	protected RoomReservation b1r1;
	protected RoomReservation b1r2;
	protected RoomReservation b1r3;
	protected Type t1;
	protected Type t2;
	protected Room r1;
	protected Room r2;
	protected Room r3;
	protected Room r4;

	@Before
	public void setUp(){
		factory = Group05FactoryImpl.init();
		bm = factory.createBookingManager();
		tm = factory.createTypeManager();
		rm = factory.createRoomManager();
		
		b1 = factory.createBooking();
		b2 = factory.createBooking();
		b3 = factory.createBooking();
		
		b1r1 = factory.createRoomReservation();
		b1r2 = factory.createRoomReservation();
		b1r3 = factory.createRoomReservation();
		
		t1 = factory.createType();
		t2 = factory.createType();
		
		r1 = factory.createRoom();
		r2 = factory.createRoom();
		r3 = factory.createRoom();
		r4 = factory.createRoom();
	}
	
	@After
	public void tearDown(){
		factory = null;
		bm.getBooking().clear();
		bm.getInitiatedRoomCheckouts().clear();
		bm.setCurrentCheckoutBooking(null);
		bm = null;
		tm.getType().clear();
		tm = null;
		rm.getRoom().clear();
		rm = null;
		
		b1.getRoomreservation().clear();
		b1 = null;
		
		b2.getRoomreservation().clear();
		b2 = null;
		
		b3.getRoomreservation().clear();
		b3 = null;
		
		b1r1 = null;
		b1r2 = null;
		b1r3 = null;
		
		t1 = null;
		t2 = null;
		
		r1 = null;
		r2 = null;
		r3 = null;
		r4 = null;
		
	}
	
	/*
	@Test
	public void testGetFreeRooms() {// MARCO
		fail("Not yet implemented");
	}

	

	@Test
	public void testAddRoomToBooking() {
		fail("Not yet implemented");
	}
	*/
	
	@Test
	public void testInitiateBooking() {
		assertEquals(-1, bm.initiateBooking(null, "20160101", "20170101", "Persson"));
		assertEquals(-1, bm.initiateBooking("Per", "20160101", "20170101", null));
		assertEquals(-1, bm.initiateBooking("", "20160101", "20170101", "Persson"));
		assertEquals(-1, bm.initiateBooking("Per", "20160101","20170101", ""));
		assertEquals(-1, bm.initiateBooking("Per", "20160101", "20150101", "Persson"));
		assertTrue(bm.initiateBooking("Per", "20160101", "20170101", "Persson") >= 0); 
		
	}
	
	
	@Test
	public void testRemoveRoomFromBooking() {
		bm.getBooking().add(b1);
		b1.setId(1);
		t1.setName("testType");
		b1r1.setType(t1);
		b1r2.setType(t1);
		b1.getRoomreservation().add(b1r1);
		b1.getRoomreservation().add(b1r2);
		
		bm.removeTypeFromBooking(1, "testType");
		
		assertFalse(b1.getRoomreservation().contains(b1r1));
		assertTrue(b1.getRoomreservation().contains(b1r2));
	}

	@Test
	public void testConfirmBooking() {//Johan
		bm.getBooking().add(b1);
		b1.getRoomreservation().add(b1r1);
		b1.getRoomreservation().add(b1r2);
		
		assertTrue(bm.confirmBooking(b1.getId()));
		assertTrue(b1.isConfirmed());
	}

	@Test
	public void testInitiateCheckoutFail(){//Johan
		prepareBookingCheckout();
		
		assertTrue(bm.initiateCheckout(-638287583) == -1);//non-existing bookingID
	}
	
	@Test
	public void testInitiateCheckout() {//Johan
		prepareBookingCheckout();

		assertTrue(bm.initiateCheckout(b1.getId()) == 200);//correct checkout
		
		assertTrue(bm.initiateCheckout(b2.getId()) == -2);//should return -2 since another checkout already is initialized
	}

	//NOTE this test may not go through if the external banking component is down
	@Test
	public void testPayDuringCheckoutSuccess() {//Johan
		prepareBookingCheckout();
		
		bm.initiateCheckout(b1.getId());
		
		//Makes sure that the user pays the booking, the rooms are checked out and roomRes are marked as paid
		assertTrue(bm.payDuringCheckout("05123321", "666", 11, 16, "Johan", "Ljungberg"));
		assertTrue(r1.getStatus() == Status.FREE);
		assertTrue(r2.getStatus() == Status.FREE);
		assertTrue(b1r1.isPaid());
		assertTrue(b1r2.isPaid());
	}

	@Test
	public void testInitiateRoomCheckoutFail(){//Johan
		prepareRoomCheckout();
		
		//Makes sure that the method returns the right number when going wrong
		assertTrue(bm.initiateRoomCheckout(101, -1) == -1);
		
		assertTrue(bm.initiateRoomCheckout(104, b1.getId()) == -2);
		
		assertTrue(bm.initiateRoomCheckout(42, b1.getId()) == -4);
	}
	
	@Test
	public void testInitiateRoomCheckoutSuccess() {//Johan
		prepareRoomCheckout();
		
		//Makes sure that the method returns the right price
		assertTrue(bm.initiateRoomCheckout(101, b1.getId()) == 100);
		
		assertTrue(bm.initiateRoomCheckout(102, b1.getId()) == 75);		
	}
	
	//NOTE this test may not go through if the external banking component is down
	@Test
	public void testPayRoomDuringCheckoutSuccess() {//Johan
		prepareRoomCheckout();
		
		bm.initiateRoomCheckout(101, b1.getId());
		
		//Makes sure the checkout is going through
		assertTrue(bm.payRoomDuringCheckout(101, "05123321", "666", 11, 16, "Johan", "Ljungberg"));
		
		//Makes sure that it isn't possible to check out one room twice
		assertFalse(bm.payRoomDuringCheckout(101, "05123321", "666", 11, 16, "Johan", "Ljungberg"));
		
		//Makes sure that only one of two rooms is checked out
		assertTrue(r1.getStatus() == Status.FREE);
		assertTrue(r2.getStatus() == Status.OCCUPIED);
		
		//Makes sure that only one of two roomRes is checked out
		assertTrue(b1r1.getCheckOut() != null);
		assertTrue(b1r2.getCheckOut() == null);
		
		//Makes sure that only one of two roomRes is marked as paid
		assertTrue(b1r1.isPaid());
		assertTrue(!b1r2.isPaid());
	}
	
	//NOTE this test may not go through if the external banking component is down
	@Test
	public void testPayRoomDuringCheckoutFail(){//Johan
		prepareRoomCheckout();
		
		bm.initiateRoomCheckout(101, b1.getId());
		
		//Makes sure that it isn't possible to pay and check out a room that isn't initiated
		assertFalse(bm.payRoomDuringCheckout(104, "05123321", "666", 11, 16, "Johan", "Ljungberg"));
	}
	
	@Test
	public void testCheckInRoomFail(){//Johan
		prepareCheckIn();
		
		//non-existing type, existing ID, nothing should happen
		int i = bm.checkInRoom("invalidType", b1.getId()); 
		
		//Makes sure that the right number is returned when going wrong
		assertTrue(i == -2);
		
		//Makes sure that no rooms are checked in
		assertTrue(	r1.getStatus() == Status.FREE &&
					r2.getStatus() == Status.FREE &&
					r3.getStatus() == Status.FREE);
		
		//existing type, non-existing ID, nothing should happen
		i = bm.checkInRoom("rightType", -7928391); 
		
		//Makes sure that the right number is returned when going wrong
		assertTrue(i == -1);
		
		//Makes sure that no rooms are checked in
		assertTrue(	r1.getStatus() == Status.FREE &&
					r2.getStatus() == Status.FREE &&
					r3.getStatus() == Status.FREE);
		
	}
	
	@Test
	public void testCheckInRoomSuccess() {//Johan
		prepareCheckIn();
		
		//Booking contains two roomRes of type "rightType"
		int i = bm.checkInRoom("rightType", b1.getId());//Right type and ID, should make things happen
		
		//Makes sure that method have returned a room number greater than 0
		assertTrue(i > 0);
		
		//Makes sure that only one roomRes have been checked in
		assertTrue(((b1.getRoomreservation().get(0).getRoom() != null && b1.getRoomreservation().get(1).getRoom() == null) ||
					(b1.getRoomreservation().get(0).getRoom() == null && b1.getRoomreservation().get(1).getRoom() != null)));
		
		//Makes sure that the room of wrong type not is checked in by the method
		assertTrue(r3.getStatus() == Status.FREE);
		
		//Makes sure that exactly 1 room of the right type is checked in
		assertTrue(((r1.getStatus() == Status.OCCUPIED && r2.getStatus() == Status.FREE && r4.getStatus() == Status.FREE) ||
					(r1.getStatus() == Status.FREE && r2.getStatus() == Status.FREE&& r4.getStatus() == Status.OCCUPIED) ||
					(r1.getStatus() == Status.FREE && r2.getStatus() == Status.OCCUPIED && r4.getStatus() == Status.FREE)));
		
		//Makes sure that a check in date is added to the checked in booking
		assertTrue(((b1.getRoomreservation().get(0).getCheckIn() != null && b1.getRoomreservation().get(1).getCheckIn() == null) ||
					(b1.getRoomreservation().get(0).getCheckIn() == null && b1.getRoomreservation().get(1).getCheckIn() != null)));
	
	}

	@Test
	public void testCheckInBookingSuccess() {//Johan
		prepareCheckIn();
		
		//Booking contains two roomRes of type "rightType"
		assertTrue(bm.checkInBooking(b1.getId()));
		
		//Makes sure that exactly 2 rooms of the right type is checked in
		assertTrue(((r1.getStatus() == Status.OCCUPIED && r2.getStatus() == Status.OCCUPIED && r4.getStatus() == Status.FREE) ||
					(r1.getStatus() == Status.OCCUPIED && r2.getStatus() == Status.FREE&& r4.getStatus() == Status.OCCUPIED) ||
					(r1.getStatus() == Status.FREE && r2.getStatus() == Status.OCCUPIED && r4.getStatus() == Status.OCCUPIED)));
		
		//Makes sure that the room with wrong type not is checked in
		assertTrue(r3.getStatus() == Status.FREE);
		
		//Makes sure that all assigns to roomRes variables is successful
		assertTrue(b1.getRoomreservation().get(0).getType().getName() == "rightType");
		assertTrue(b1.getRoomreservation().get(1).getType().getName() == "rightType");
		assertTrue(b1.getRoomreservation().get(0).getRoom().getType().getName() == "rightType");
		assertTrue(b1.getRoomreservation().get(1).getRoom().getType().getName() == "rightType");
		assertTrue(b1.getRoomreservation().get(0).getCheckIn() != null);
		assertTrue(b1.getRoomreservation().get(1).getCheckIn() != null);
		
		
	}

	@Test
	public void testCheckInBookingFail(){//Johan
		prepareCheckIn();
		
		//Makes sure that a booking with invalid id can't check in
		assertFalse(bm.checkInBooking(-1234));
		
		//Makes sure that no room is checked in
		assertTrue(r1.getStatus() == Status.FREE);
		assertTrue(r2.getStatus() == Status.FREE);
		assertTrue(r3.getStatus() == Status.FREE);
		assertTrue(r4.getStatus() == Status.FREE);
				
		//Makes sure that the roomRes aren't changed
		assertTrue(b1.getRoomreservation().get(0).getRoom() == null);
		assertTrue(b1.getRoomreservation().get(1).getRoom() == null);
		assertTrue(b1.getRoomreservation().get(0).getCheckIn() == null);
		assertTrue(b1.getRoomreservation().get(1).getCheckIn() == null);
		
		
	}
	
	@Test
	public void testAddTypeToBooking() {		
		tm.getType().add(t1);
		rm.getRoom().add(r1);
		t1.setName("testRoom567");
		r1.setType(t1);
		b1.setId(1);
		b1.setDateIn(Utils.getDateFromString("20170101"));
		b1.setDateOut(Utils.getDateFromString("20170115"));
		bm.getBooking();
		bm.getAllBookings().add(b1);
		assertTrue(bm.addTypeToBooking(1, "testRoom567"));
		String checkName = "";
		
		for(int i = 0; i < b1.getRoomreservation().size(); i++){
			if(b1.getRoomreservation().get(i).getType() != null){
				checkName = b1.getRoomreservation().get(i).getType().getName();
			}
		}
		
		assertEquals(checkName,"testRoom567");
	}

	@Test
	public void testSetBookingExpectedDates() {
		bm.getBooking().add(b1);
		b1.setId(1);
		bm.setBookingExpectedDates(b1.getId(), "20170101", "20170115");
		assertEquals(Utils.getStringDate(b1.getDateIn()), "20170101");
		assertEquals(Utils.getStringDate(b1.getDateOut()), "20170115");
	}

	@Test
	public void testCancelBooking() {
		b1.setId(1);
		b2.setId(2);
		b3.setId(3);
		bm.getBooking();
		bm.getAllBookings().add(b1);
		bm.getAllBookings().add(b2);
		bm.getAllBookings().add(b3);
		bm.cancelBooking(3);
		bm.cancelBooking(1);
		
		assertTrue(!bm.getAllBookings().contains(b1));
		assertTrue(bm.getAllBookings().contains(b2));
		assertTrue(!bm.getAllBookings().contains(b3));
		
	}

	/*
	@Test
	public void testAddCostToRoom() {
		fail("Not yet implemented");
	}
	*/
	
	@Test
	public void testGetCheckIns() { //Joakim
		b1.getRoomreservation().add(b1r1);
		b1.getRoomreservation().add(b1r2);
		bm.getBooking().add(b1);
		
	
		b1r1.setCheckIn(Utils.getDateFromString("20161215"));
		b1r1.setCheckOut(Utils.getDateFromString("20171213"));
		
		b1r2.setCheckIn(Utils.getDateFromString("20161213"));
		b1r2.setCheckOut(Utils.getDateFromString("20161216"));
		
		assertEquals(1,bm.getCheckIns("20161212", "20161214").size());
		assertEquals(2,bm.getCheckIns("20161212", "20161220").size());
	}
	
	@Test
	public void testGetCheckOuts(){ //Joakim
		b1.getRoomreservation().add(b1r1);
		b1.getRoomreservation().add(b1r2);
		bm.getBooking().add(b1);
		
		b1r1.setCheckIn(Utils.getDateFromString("20161215"));
		b1r1.setCheckOut(Utils.getDateFromString("20171213"));
		
		b1r2.setCheckIn(Utils.getDateFromString("20161213"));
		b1r2.setCheckOut(Utils.getDateFromString("20161216"));
		
		
		assertEquals(1,bm.getCheckOuts("20161212", "20161220").size());
		assertEquals(2,bm.getCheckOuts("20161212", "20171220").size());
	}
	
	@Test
	public void testGetOccupiedRooms(){
		b1.getRoomreservation().add(b1r1);
		b1.getRoomreservation().add(b1r2);
		bm.getBooking().add(b1);
		
		b1r1.setCheckIn(Utils.getDateFromString("20161215"));
		b1r1.setCheckOut(Utils.getDateFromString("20171213"));
		
		b1r2.setCheckIn(Utils.getDateFromString("20161213"));
		b1r2.setCheckOut(Utils.getDateFromString("20161217"));
		
		//Test before
		assertEquals(1,bm.getOccupiedRooms(Utils.getDateFromString("20161214")).size());
		//Test equal
		assertEquals(2,bm.getOccupiedRooms(Utils.getDateFromString("20161215")).size());
		//Test after
		assertEquals(1,bm.getOccupiedRooms(Utils.getDateFromString("20161218")).size());
	}
	
	private void prepareRoomCheckout(){//Johan
		b1.getRoomreservation().add(b1r1);
		b1.getRoomreservation().add(b1r2);
		
		b1.setId(1);
		
		bm.getBooking().add(b1);
		
		t1.setPrice(100);
		t2.setPrice(75);
		
		r1.setStatus(Status.OCCUPIED);
		r1.setNumber(101);
		r2.setStatus(Status.OCCUPIED);
		r2.setNumber(102);
		r3.setStatus(Status.FREE);
		r3.setNumber(103);
		r4.setStatus(Status.FREE);
		r4.setNumber(104);
		
		rm.getRoom().add(r1);
		rm.getRoom().add(r2);
		rm.getRoom().add(r3);
		rm.getRoom().add(r4);
		
		b1r1.setRoom(r1);
		b1r1.setCheckIn(Utils.getDateFromString("20161010"));
		b1r1.setBookingId(b1.getId());
		b1r1.setIsPaid(false);
		b1r1.setType(t1);
		
		b1r2.setRoom(r2);
		b1r2.setCheckIn(Utils.getDateFromString("20161010"));
		b1r2.setBookingId(b1.getId());
		b1r2.setIsPaid(false);
		b1r2.setType(t2);
	}
	
	private void prepareCheckIn(){//Johan
		bm.getBooking().add(b1);
		
		b1r1.setType(t1);
		b1r2.setType(t1);
		
		b1.getRoomreservation().add(b1r1);
		b1.getRoomreservation().add(b1r2);
		
		tm.getType().add(t1);
		tm.getType().add(t2);
		
		t1.setName("rightType");
		t2.setName("wrongType");
		
		r1.setStatus(Status.FREE);
		r1.setType(t1);
		r1.setNumber(101);
		
		r2.setStatus(Status.FREE);
		r2.setType(t1);
		r2.setNumber(102);
		
		r3.setStatus(Status.FREE);
		r3.setType(t2);
		r3.setNumber(666);
		
		r4.setStatus(Status.FREE);
		r4.setType(t1);
		r4.setNumber(103);
		
		rm.getRoom().add(r1);
		rm.getRoom().add(r2);
		rm.getRoom().add(r3);
		rm.getRoom().add(r4);
	}
	
	private void prepareBookingCheckout(){//Johan
		b1.getRoomreservation().add(b1r1);
		b1.getRoomreservation().add(b1r2);
		
		b2.getRoomreservation().add(b1r3);
		
		b1.setId(1);
		b2.setId(2);
		
		bm.getBooking().add(b1);
		bm.getBooking().add(b2);
		
		t1.setPrice(100);
		t2.setPrice(75);
		
		r1.setStatus(Status.OCCUPIED);
		r2.setStatus(Status.OCCUPIED);
		r3.setStatus(Status.FREE);
		r4.setStatus(Status.FREE);
		
		b1r1.setRoom(r1);
		b1r1.setCheckIn(Utils.getDateFromString("20161010"));
		b1r1.setBookingId(b1.getId());
		b1r1.setIsPaid(false);
		b1r1.setType(t1);
		
		b1r2.setRoom(r2);
		b1r2.setCheckIn(Utils.getDateFromString("20161010"));
		b1r2.setBookingId(b1.getId());
		b1r2.setIsPaid(false);
		b1r2.setType(t1);
		
		b1r3.setRoom(r3);
		b1r3.setCheckIn(Utils.getDateFromString("20161014"));
		b1r3.setBookingId(b2.getId());
		b1r3.setIsPaid(false);
		b1r3.setType(t2);
	}
	

}
