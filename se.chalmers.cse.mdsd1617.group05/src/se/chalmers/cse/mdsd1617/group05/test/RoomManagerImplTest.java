package se.chalmers.cse.mdsd1617.group05.test;

import static org.junit.Assert.*;
import static org.junit.Assume.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group05.RoomManager;
import se.chalmers.cse.mdsd1617.group05.Type;
import se.chalmers.cse.mdsd1617.group05.TypeManager;
import se.chalmers.cse.mdsd1617.group05.Booking;
import se.chalmers.cse.mdsd1617.group05.BookingManager;
import se.chalmers.cse.mdsd1617.group05.Group05Factory;
import se.chalmers.cse.mdsd1617.group05.Room;
import se.chalmers.cse.mdsd1617.group05.Status;
import se.chalmers.cse.mdsd1617.group05.impl.Group05FactoryImpl;
import se.chalmers.cse.mdsd1617.group05.impl.RoomManagerImpl;
import se.chalmers.cse.mdsd1617.group05.impl.TypeManagerImpl;

public class RoomManagerImplTest {
	private static RoomManager rm;
	protected static Group05Factory factory;
	private Type t; 
	
	private static final int TEST_ROOMNUMBER = 100;
	private static final String TEST_ROOMTYPE = "helloworld";
	
	protected TypeManager tm;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		factory = Group05FactoryImpl.init();
		rm = factory.createRoomManager();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() {
		t = RoomManagerImpl.getFactory().createType();
		t.setName(TEST_ROOMTYPE);
		t.setNumberOfBeds(2);
		t.setPrice(100);
		
		//add predefined Type t with name "helloworld" in TypeManager
		RoomManagerImpl.getiTypeManager().getTypes().add(t);
		tm = factory.createTypeManager();
		
		tm.addRoomType("default", 30, 2);
		
	}

	@After
	public void tearDown() {
		rm.clearRooms();
		tm.clearTypes();
	}
	
	@Test
	public void testIfReferencesHaveBeenInitiated() {
		assertNotNull("Reference of factory is not initiated", RoomManagerImpl.getFactory());
		assertSame("Reference of iTypeManager is not the right one", RoomManagerImpl.getiTypeManager(), factory.createTypeManager());
		assertSame("RoomManager is not the right one", rm, factory.createRoomManager());
	}

	/**
	 * test change the type of a non-existing room.
	 * expect false
	 * @author kevin
	 */
	@Test
	public void testChangeRoomTypeOfRoomWithAbsentRoom() {
		//ensure the room does't exist.
		if(rm.fetchRoomById(TEST_ROOMNUMBER) != null)
			rm.removeRoom(TEST_ROOMNUMBER);
		
		assertFalse(rm.changeRoomTypeOfRoom(TEST_ROOMNUMBER, TEST_ROOMTYPE));
	}
	
	/**
	 * test change the type of a normal room
	 * expect true
	 * @author kevin
	 */
	@Test
	public void testChangeRoomType() {
		//add a new type
		Type newType = factory.createType();
		newType.setName("newType");
		RoomManagerImpl.getiTypeManager().getTypes().add(newType);
		//test if the adding has been succesfully added by calling addRoom with the new type.
		assertTrue(rm.addRoom(TEST_ROOMNUMBER-10, "newType"));
		
		//make sure that room with specified room number exists.
		if(rm.fetchRoomById(TEST_ROOMNUMBER) == null)
			rm.addRoom(TEST_ROOMNUMBER, TEST_ROOMTYPE);
		assertNotNull(rm.fetchRoomById(TEST_ROOMNUMBER));

		assertTrue(rm.changeRoomTypeOfRoom(TEST_ROOMNUMBER, "newType"));
		tearDown();
	}
	
	/**
	 * test if we can change a room with non-existing type
	 * expect false
	 * @author kevin
	 */
	@Test
	public void testChangeRoomTypeWithNonExistingType() {
		//make sure that room with specified room number exists.
		if(rm.fetchRoomById(TEST_ROOMNUMBER) == null)
			rm.addRoom(TEST_ROOMNUMBER, TEST_ROOMTYPE);
		assertNotNull(rm.fetchRoomById(TEST_ROOMNUMBER));
		
		assertFalse(rm.changeRoomTypeOfRoom(0, "definatelyNotAType"));
		tearDown();
	}

	/**
	 * test to make sure iTypeManager works as expected.
	 * @author kevin
	 */
	@Test
	public void testIfTypeWorks() {
		boolean isPresent = false;
		
		//check if there's Type with name "hi" in Typemanager, expect false
		for(Type t : RoomManagerImpl.getiTypeManager().getTypes()) {
			if(t.getName().equalsIgnoreCase("hi"))
				isPresent = true;
		}
		assertFalse(isPresent);
		
		//check if there's Type with name "helloworld" in Typemanager, expect true
		for(Type t : RoomManagerImpl.getiTypeManager().getTypes()) {
			if(t.getName().equalsIgnoreCase("helloworld"))
				isPresent = true;
		}
		assertTrue(isPresent);
		
	}
	
	/**
	 * test if the fetch method works in normal scenario.
	 * expect return a reference of a room
	 * @author kevin
	 */
	@Test
	public void testFetchRoomById() {
		// create and add a dummy room object to the list
		Room r = RoomManagerImpl.getFactory().createRoom();
		r.setNumber(TEST_ROOMNUMBER-1);
		r.setType(t);
		rm.getRooms().add(r);
		
		//checking the result 
		assertNotNull(rm.fetchRoomById(TEST_ROOMNUMBER-1));
		assertSame(r, rm.fetchRoomById(TEST_ROOMNUMBER-1));
		
		tearDown();
	}
	
	/**
	 * test if the fetch method still fetches anything when the specified ID is absent
	 * expect return null
	 * @author kevin
	 */
	@Test
	public void testFetchRoomByIdDoesNotExist() {
		//ensure the room dosen't exist.
		boolean found = false;
		
		for(Room r : rm.getRooms()) {
			if(TEST_ROOMNUMBER-2 == r.getNumber())
				found = true;
		}
		
		assumeFalse(found);
			
		//trying to find a room with specified ID in the list
		assertNull(rm.fetchRoomById(TEST_ROOMNUMBER-2));
		
		tearDown();
	}
	
	/**
	 * testing when adding a new room with an already existing type
	 * expect true
	 * @author kevin
	 */
	@Test
	public void testAddRoom() {
		//make sure that a room with roomNum doesn't exist
		Room sth = null;
		
		for(Room r : rm.getRooms()) {
			if(TEST_ROOMNUMBER == r.getNumber()) {
				sth = r;
				break;
			}				
		}
		
		if (sth != null) {
			rm.getRooms().remove(sth);
		}
		
		//check if the method call success.
		assertTrue(rm.addRoom(TEST_ROOMNUMBER, TEST_ROOMTYPE));
		
		//chekc if the added room is able to be found in the list
		boolean found = false;
		for(Room r : rm.getRooms()) {
			if(TEST_ROOMNUMBER == r.getNumber() && r.getType().getName().equals(TEST_ROOMTYPE))
				found = true;
		}
		
		assertTrue(found);
		
		tearDown();
	}
	
	/**
	 * testing adding a new room with a type that doesn't create in advance.
	 * expect false
	 * @author kevin
	 */
	@Test
	public void testAddRoomWithAbsentType() {
		assertFalse(rm.addRoom(TEST_ROOMNUMBER, "hi"));
		tearDown();
	}
	
	/**
	 * testing adding a new room with existing room number.
	 * expect return false
	 * @author kevin
	 */
	@Test
	public void testAddRoomWithDuplicatedNumber() {
		//check if room with same number can be added twice
		assertTrue(rm.addRoom(TEST_ROOMNUMBER+1, TEST_ROOMTYPE));
		assertFalse(rm.addRoom(TEST_ROOMNUMBER+1, TEST_ROOMTYPE));
		tearDown();
	}

	/**
	 * testing remove a non-existing room.
	 * expect false
	 * @author kevin
	 */
	@Test
	public void testRemoveRoomDoesNotExist() {
		assertFalse(rm.removeRoom(TEST_ROOMNUMBER-3));
		tearDown();
	}
	
	/**
	 * testing remove a room in normal scenario.
	 * expect true
	 * @author kevin
	 */
	@Test
	public void testRemoveRoom() {
		//remove an existing room with specified ID from list
		rm.addRoom(TEST_ROOMNUMBER-4, TEST_ROOMTYPE);
		assertTrue(rm.removeRoom(TEST_ROOMNUMBER-4));
		
		//check absence of that room
		assertNull(rm.fetchRoomById(TEST_ROOMNUMBER-4));
		tearDown();
	}

	@Test //Isaac
	public void testBlockRoom() {
		setUp();
		
		rm.addRoom(0, "default");
		rm.addRoom(1, "default");
		rm.addRoom(2, "default");
		
		rm.fetchRoomById(0).setStatus(Status.FREE);
		rm.fetchRoomById(1).setStatus(Status.BLOCKED);
		rm.fetchRoomById(2).setStatus(Status.OCCUPIED);
		
		assertTrue(rm.blockRoom(0));
		assertFalse(rm.blockRoom(1));
		assertFalse(rm.blockRoom(2));
		
		tearDown();
	}

	@Test //Isaac
	public void testUnblockRoom() {
		setUp();
		
		rm.addRoom(0, "default");
		rm.addRoom(1, "default");
		rm.addRoom(2, "default");
		
		rm.fetchRoomById(0).setStatus(Status.FREE);
		rm.fetchRoomById(1).setStatus(Status.BLOCKED);
		rm.fetchRoomById(2).setStatus(Status.OCCUPIED);
		
		assertFalse(rm.unblockRoom(0));
		assertTrue(rm.unblockRoom(1));
		assertFalse(rm.unblockRoom(2));
		
		tearDown();
	}

	@Test // Isaac
	public void testClearRooms() {
		setUp();
		rm.addRoom(0, "default");
		rm.addRoom(1, "default");
		rm.addRoom(2, "default");
		rm.addRoom(3, "default");
		rm.addRoom(4, "default");
		
		assertTrue(rm.getRooms().size() == 5);
		
		rm.clearRooms();
		
		assertTrue(rm.getRooms().size() == 0);
		
	}
	
	@Test
	public void testGetUnblockedRooms(){ // MARCO
		RoomManager rm = factory.createRoomManager();
		Type type1 = factory.createType();
		
		type1.setName("SingleRoom");
		type1.setNumberOfBeds(1);
		type1.setPrice(20.0);
		Type type2 = factory.createType();
		type2.setName("DoubleRoom");
		type2.setNumberOfBeds(2);
		type2.setPrice(35.0);
		Type type3 = factory.createType();
		type3.setName("Suite");
		type3.setNumberOfBeds(2);
		type3.setPrice(80.0);
		
		Room room1 = factory.createRoom();
		Room room2 = factory.createRoom();
		Room room3 = factory.createRoom();
		
		room1.setStatus(Status.BLOCKED);
		room1.setType(type1);
		
		room2.setStatus(Status.FREE);
		room2.setType(type2);
		
		room3.setStatus(Status.OCCUPIED);
		room3.setType(type3);
		
		
		
		fail("Not yet implemented"); // TODO
	}

}
