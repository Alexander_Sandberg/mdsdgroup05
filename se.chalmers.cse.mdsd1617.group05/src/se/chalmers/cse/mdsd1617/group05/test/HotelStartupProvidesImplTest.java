package se.chalmers.cse.mdsd1617.group05.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group05.Booking;
import se.chalmers.cse.mdsd1617.group05.BookingManager;
import se.chalmers.cse.mdsd1617.group05.Group05Factory;
import se.chalmers.cse.mdsd1617.group05.HotelStartupProvides;
import se.chalmers.cse.mdsd1617.group05.Room;
import se.chalmers.cse.mdsd1617.group05.RoomManager;
import se.chalmers.cse.mdsd1617.group05.Type;
import se.chalmers.cse.mdsd1617.group05.TypeManager;
import se.chalmers.cse.mdsd1617.group05.impl.Group05FactoryImpl;

public class HotelStartupProvidesImplTest {
	
	protected Group05Factory factory;
	protected HotelStartupProvides hs;
	protected RoomManager rm;
	protected TypeManager tm;
	
	@Before // Isaac
	public void setup() {
		factory = Group05FactoryImpl.init();
		hs = factory.createHotelStartupProvides();
		rm = factory.createRoomManager();
		tm = factory.createTypeManager();
	}
	
	@After //Isaac
	public void tearDown() {
		factory = null;
		hs = null;
		rm = null;
		tm = null;
	}
	
	@Test //Isaac
	public void testStartup() {
		setup();
		hs.startup(8);
		
		assertTrue(hs.getBookingmanager().getBooking().size() == 0);
		assertTrue(hs.getRoommanager().getRoom().size() == 8);
		assertTrue(hs.getRoommanager().getFreeRooms().size() == 8);
		assertTrue(hs.getTypemanager().getType().size() == 1);
		assertTrue(hs.getTypemanager().getType().get(0).getName() == "default");
		assertTrue(hs.getTypemanager().getType().get(0).getNumberOfBeds() == 2);
		assertTrue(hs.getRoommanager().getRoom().get(0).getType().getName() == "default");
		assertTrue(hs.getRoommanager().getRoom().get(5).getType().getPrice() == 30);
		
		hs.startup(100);
		
		assertTrue(hs.getRoommanager().getRoom().size() == 100);
		assertTrue(hs.getRoommanager().getFreeRooms().size() == 100);
		assertTrue(hs.getTypemanager().getType().get(0).getName() == "default");
		assertTrue(hs.getTypemanager().getType().get(0).getNumberOfBeds() == 2);
		assertTrue(hs.getRoommanager().getRoom().get(0).getType().getName() == "default");
		assertTrue(hs.getRoommanager().getRoom().get(5).getType().getPrice() == 30);
		assertTrue(rm.getFreeRooms().size() == 100);
		
		hs.startup(1);
		assertTrue(hs.getRoommanager().getRoom().size() == 1);
		assertTrue(hs.getRoommanager().getFreeRooms().size() == 1);
		
		assertTrue(rm.getFreeRooms().size() == 1);
		assertTrue(tm.getType().size() == 1);
		assertTrue(tm.getTypes().size() == 1);
		
		tearDown();
	}

}
