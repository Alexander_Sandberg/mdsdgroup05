/**
 */
package se.chalmers.cse.mdsd1617.group05;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.RoomManager#getRoom <em>Room</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getRoomManager()
 * @model
 * @generated
 */
public interface RoomManager extends IRoomAdministrator, IRoomManager {
	/**
	 * Returns the value of the '<em><b>Room</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group05.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getRoomManager_Room()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	EList<Room> getRoom();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false"
	 * @generated
	 */
	boolean clearRooms();

} // RoomManager
