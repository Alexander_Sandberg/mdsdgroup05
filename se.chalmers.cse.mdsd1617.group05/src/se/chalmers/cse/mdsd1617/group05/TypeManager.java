/**
 */
package se.chalmers.cse.mdsd1617.group05;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.TypeManager#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getTypeManager()
 * @model
 * @generated
 */
public interface TypeManager extends ITypeAdministrator, ITypeManager {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group05.Type}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getTypeManager_Type()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	EList<Type> getType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false"
	 * @generated
	 */
	boolean clearTypes();

} // TypeManager
