/**
 */
package se.chalmers.cse.mdsd1617.group05;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.HotelStartupProvides#getRoommanager <em>Roommanager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.HotelStartupProvides#getBookingmanager <em>Bookingmanager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.HotelStartupProvides#getTypemanager <em>Typemanager</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getHotelStartupProvides()
 * @model
 * @generated
 */
public interface HotelStartupProvides extends IHotelStartupProvides {
	/**
	 * Returns the value of the '<em><b>Roommanager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roommanager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roommanager</em>' reference.
	 * @see #setRoommanager(RoomManager)
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getHotelStartupProvides_Roommanager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomManager getRoommanager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group05.HotelStartupProvides#getRoommanager <em>Roommanager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Roommanager</em>' reference.
	 * @see #getRoommanager()
	 * @generated
	 */
	void setRoommanager(RoomManager value);

	/**
	 * Returns the value of the '<em><b>Bookingmanager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bookingmanager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bookingmanager</em>' reference.
	 * @see #setBookingmanager(BookingManager)
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getHotelStartupProvides_Bookingmanager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	BookingManager getBookingmanager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group05.HotelStartupProvides#getBookingmanager <em>Bookingmanager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bookingmanager</em>' reference.
	 * @see #getBookingmanager()
	 * @generated
	 */
	void setBookingmanager(BookingManager value);

	/**
	 * Returns the value of the '<em><b>Typemanager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Typemanager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Typemanager</em>' reference.
	 * @see #setTypemanager(TypeManager)
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getHotelStartupProvides_Typemanager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	TypeManager getTypemanager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group05.HotelStartupProvides#getTypemanager <em>Typemanager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Typemanager</em>' reference.
	 * @see #getTypemanager()
	 * @generated
	 */
	void setTypemanager(TypeManager value);

} // HotelStartupProvides
