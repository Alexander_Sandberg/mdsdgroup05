/**
 */
package se.chalmers.cse.mdsd1617.group05;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Booking</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.Booking#getRoomreservation <em>Roomreservation</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.Booking#getId <em>Id</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.Booking#getDateIn <em>Date In</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.Booking#getDateOut <em>Date Out</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.Booking#getFirstName <em>First Name</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.Booking#getLastName <em>Last Name</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.Booking#isConfirmed <em>Is Confirmed</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getBooking()
 * @model
 * @generated
 */
public interface Booking extends EObject {
	/**
	 * Returns the value of the '<em><b>Roomreservation</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group05.RoomReservation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roomreservation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roomreservation</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getBooking_Roomreservation()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	EList<RoomReservation> getRoomreservation();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(int)
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getBooking_Id()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group05.Booking#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

	/**
	 * Returns the value of the '<em><b>Date In</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date In</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date In</em>' attribute.
	 * @see #setDateIn(Date)
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getBooking_DateIn()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Date getDateIn();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group05.Booking#getDateIn <em>Date In</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date In</em>' attribute.
	 * @see #getDateIn()
	 * @generated
	 */
	void setDateIn(Date value);

	/**
	 * Returns the value of the '<em><b>Date Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date Out</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date Out</em>' attribute.
	 * @see #setDateOut(Date)
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getBooking_DateOut()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Date getDateOut();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group05.Booking#getDateOut <em>Date Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date Out</em>' attribute.
	 * @see #getDateOut()
	 * @generated
	 */
	void setDateOut(Date value);

	/**
	 * Returns the value of the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Name</em>' attribute.
	 * @see #setFirstName(String)
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getBooking_FirstName()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getFirstName();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group05.Booking#getFirstName <em>First Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Name</em>' attribute.
	 * @see #getFirstName()
	 * @generated
	 */
	void setFirstName(String value);

	/**
	 * Returns the value of the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Name</em>' attribute.
	 * @see #setLastName(String)
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getBooking_LastName()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getLastName();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group05.Booking#getLastName <em>Last Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Name</em>' attribute.
	 * @see #getLastName()
	 * @generated
	 */
	void setLastName(String value);

	/**
	 * Returns the value of the '<em><b>Is Confirmed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Confirmed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Confirmed</em>' attribute.
	 * @see #setIsConfirmed(boolean)
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getBooking_IsConfirmed()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	boolean isConfirmed();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group05.Booking#isConfirmed <em>Is Confirmed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Confirmed</em>' attribute.
	 * @see #isConfirmed()
	 * @generated
	 */
	void setIsConfirmed(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true" ordered="false"
	 * @generated
	 */
	double getTotalPrice();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true" ordered="false"
	 * @generated
	 */
	String getStringDateIn();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true" ordered="false"
	 * @generated
	 */
	String getStringDateOut();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" amountRequired="true" amountOrdered="false" roomNumRequired="true" roomNumOrdered="false" descriptionRequired="true" descriptionOrdered="false"
	 * @generated
	 */
	boolean addCostToRoom(double amount, int roomNum, String description);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	boolean addRoomReservation(Type roomType);

} // Booking
