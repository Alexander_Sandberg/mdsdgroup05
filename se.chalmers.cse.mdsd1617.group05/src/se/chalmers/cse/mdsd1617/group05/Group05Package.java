/**
 */
package se.chalmers.cse.mdsd1617.group05;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group05.Group05Factory
 * @model kind="package"
 * @generated
 */
public interface Group05Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "group05";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group05.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group05";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Group05Package eINSTANCE = se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides <em>IHotel Customer Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getIHotelCustomerProvides()
	 * @generated
	 */
	int IHOTEL_CUSTOMER_PROVIDES = 0;

	/**
	 * The number of structural features of the '<em>IHotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = 0;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = 1;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = 2;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = 3;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = 4;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = 5;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = 6;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = 7;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = 8;

	/**
	 * The number of operations of the '<em>IHotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT = 9;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.impl.FreeRoomTypesDTOImpl <em>Free Room Types DTO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.impl.FreeRoomTypesDTOImpl
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getFreeRoomTypesDTO()
	 * @generated
	 */
	int FREE_ROOM_TYPES_DTO = 1;

	/**
	 * The feature id for the '<em><b>Room Type Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__ROOM_TYPE_DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Num Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__NUM_BEDS = 1;

	/**
	 * The feature id for the '<em><b>Price Per Night</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__PRICE_PER_NIGHT = 2;

	/**
	 * The feature id for the '<em><b>Num Free Rooms</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__NUM_FREE_ROOMS = 3;

	/**
	 * The number of structural features of the '<em>Free Room Types DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Free Room Types DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelStartupProvides <em>IHotel Startup Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelStartupProvides
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getIHotelStartupProvides()
	 * @generated
	 */
	int IHOTEL_STARTUP_PROVIDES = 2;

	/**
	 * The number of structural features of the '<em>IHotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES___STARTUP__INT = 0;

	/**
	 * The number of operations of the '<em>IHotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelReceptionist <em>IHotel Receptionist</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelReceptionist
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getIHotelReceptionist()
	 * @generated
	 */
	int IHOTEL_RECEPTIONIST = 3;

	/**
	 * The number of structural features of the '<em>IHotel Receptionist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_FEATURE_COUNT = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___GET_FREE_ROOMS__INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___ADD_ROOM_TO_BOOKING__STRING_INT = IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___CONFIRM_BOOKING__INT = IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___INITIATE_CHECKOUT__INT = IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___INITIATE_ROOM_CHECKOUT__INT_INT = IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___CHECK_IN_ROOM__STRING_INT = IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___CHECK_IN_BOOKING__INT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Remove Type From Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___REMOVE_TYPE_FROM_BOOKING__INT_STRING = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Set Booking Expected Dates</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___SET_BOOKING_EXPECTED_DATES__INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___CANCEL_BOOKING__INT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Get All Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___GET_ALL_BOOKINGS = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___GET_OCCUPIED_ROOMS__DATE = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___GET_CHECK_INS__STRING_STRING = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Get Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___GET_CHECK_OUTS__STRING_STRING = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Add Cost To Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___ADD_COST_TO_ROOM__INT_INT_STRING_DOUBLE = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Add Type To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST___ADD_TYPE_TO_BOOKING__INT_STRING = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 9;

	/**
	 * The number of operations of the '<em>IHotel Receptionist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_OPERATION_COUNT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 10;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.impl.TypeImpl <em>Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.impl.TypeImpl
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getType()
	 * @generated
	 */
	int TYPE = 6;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.impl.BookingImpl <em>Booking</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.impl.BookingImpl
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getBooking()
	 * @generated
	 */
	int BOOKING = 4;

	/**
	 * The feature id for the '<em><b>Roomreservation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__ROOMRESERVATION = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__ID = 1;

	/**
	 * The feature id for the '<em><b>Date In</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__DATE_IN = 2;

	/**
	 * The feature id for the '<em><b>Date Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__DATE_OUT = 3;

	/**
	 * The feature id for the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__FIRST_NAME = 4;

	/**
	 * The feature id for the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__LAST_NAME = 5;

	/**
	 * The feature id for the '<em><b>Is Confirmed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__IS_CONFIRMED = 6;

	/**
	 * The number of structural features of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_FEATURE_COUNT = 7;

	/**
	 * The operation id for the '<em>Get Total Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_TOTAL_PRICE = 0;

	/**
	 * The operation id for the '<em>Get String Date In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_STRING_DATE_IN = 1;

	/**
	 * The operation id for the '<em>Get String Date Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_STRING_DATE_OUT = 2;

	/**
	 * The operation id for the '<em>Add Cost To Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___ADD_COST_TO_ROOM__DOUBLE_INT_STRING = 3;

	/**
	 * The operation id for the '<em>Add Room Reservation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___ADD_ROOM_RESERVATION__TYPE = 4;

	/**
	 * The number of operations of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_OPERATION_COUNT = 5;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.impl.RoomReservationImpl <em>Room Reservation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.impl.RoomReservationImpl
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getRoomReservation()
	 * @generated
	 */
	int ROOM_RESERVATION = 5;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Room</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION__ROOM = 1;

	/**
	 * The feature id for the '<em><b>Check In</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION__CHECK_IN = 2;

	/**
	 * The feature id for the '<em><b>Check Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION__CHECK_OUT = 3;

	/**
	 * The feature id for the '<em><b>Is Paid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION__IS_PAID = 4;

	/**
	 * The feature id for the '<em><b>Booking Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION__BOOKING_ID = 5;

	/**
	 * The feature id for the '<em><b>Extracost</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION__EXTRACOST = 6;

	/**
	 * The number of structural features of the '<em>Room Reservation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION_FEATURE_COUNT = 7;

	/**
	 * The operation id for the '<em>Get Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION___GET_PRICE = 0;

	/**
	 * The operation id for the '<em>Add Extra Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION___ADD_EXTRA_COST__DOUBLE_STRING = 1;

	/**
	 * The number of operations of the '<em>Room Reservation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION_OPERATION_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Number Of Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__NUMBER_OF_BEDS = 1;

	/**
	 * The feature id for the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__PRICE = 2;

	/**
	 * The number of structural features of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.impl.RoomImpl <em>Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.impl.RoomImpl
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 7;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__STATUS = 1;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__NUMBER = 2;

	/**
	 * The number of structural features of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.impl.ExtraCostImpl <em>Extra Cost</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.impl.ExtraCostImpl
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getExtraCost()
	 * @generated
	 */
	int EXTRA_COST = 8;

	/**
	 * The feature id for the '<em><b>Amount</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_COST__AMOUNT = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_COST__DESCRIPTION = 1;

	/**
	 * The number of structural features of the '<em>Extra Cost</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_COST_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Extra Cost</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_COST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.impl.BookingManagerImpl <em>Booking Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.impl.BookingManagerImpl
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getBookingManager()
	 * @generated
	 */
	int BOOKING_MANAGER = 9;

	/**
	 * The feature id for the '<em><b>Booking</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER__BOOKING = IHOTEL_RECEPTIONIST_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Current Checkout Booking</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER__CURRENT_CHECKOUT_BOOKING = IHOTEL_RECEPTIONIST_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Initiated Room Checkouts</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER__INITIATED_ROOM_CHECKOUTS = IHOTEL_RECEPTIONIST_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Booking Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER_FEATURE_COUNT = IHOTEL_RECEPTIONIST_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___GET_FREE_ROOMS__INT_STRING_STRING = IHOTEL_RECEPTIONIST___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IHOTEL_RECEPTIONIST___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___ADD_ROOM_TO_BOOKING__STRING_INT = IHOTEL_RECEPTIONIST___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CONFIRM_BOOKING__INT = IHOTEL_RECEPTIONIST___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___INITIATE_CHECKOUT__INT = IHOTEL_RECEPTIONIST___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = IHOTEL_RECEPTIONIST___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___INITIATE_ROOM_CHECKOUT__INT_INT = IHOTEL_RECEPTIONIST___INITIATE_ROOM_CHECKOUT__INT_INT;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = IHOTEL_RECEPTIONIST___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CHECK_IN_ROOM__STRING_INT = IHOTEL_RECEPTIONIST___CHECK_IN_ROOM__STRING_INT;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CHECK_IN_BOOKING__INT = IHOTEL_RECEPTIONIST___CHECK_IN_BOOKING__INT;

	/**
	 * The operation id for the '<em>Remove Type From Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___REMOVE_TYPE_FROM_BOOKING__INT_STRING = IHOTEL_RECEPTIONIST___REMOVE_TYPE_FROM_BOOKING__INT_STRING;

	/**
	 * The operation id for the '<em>Set Booking Expected Dates</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___SET_BOOKING_EXPECTED_DATES__INT_STRING_STRING = IHOTEL_RECEPTIONIST___SET_BOOKING_EXPECTED_DATES__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CANCEL_BOOKING__INT = IHOTEL_RECEPTIONIST___CANCEL_BOOKING__INT;

	/**
	 * The operation id for the '<em>Get All Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___GET_ALL_BOOKINGS = IHOTEL_RECEPTIONIST___GET_ALL_BOOKINGS;

	/**
	 * The operation id for the '<em>Get Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___GET_OCCUPIED_ROOMS__DATE = IHOTEL_RECEPTIONIST___GET_OCCUPIED_ROOMS__DATE;

	/**
	 * The operation id for the '<em>Get Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___GET_CHECK_INS__STRING_STRING = IHOTEL_RECEPTIONIST___GET_CHECK_INS__STRING_STRING;

	/**
	 * The operation id for the '<em>Get Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___GET_CHECK_OUTS__STRING_STRING = IHOTEL_RECEPTIONIST___GET_CHECK_OUTS__STRING_STRING;

	/**
	 * The operation id for the '<em>Add Cost To Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___ADD_COST_TO_ROOM__INT_INT_STRING_DOUBLE = IHOTEL_RECEPTIONIST___ADD_COST_TO_ROOM__INT_INT_STRING_DOUBLE;

	/**
	 * The operation id for the '<em>Add Type To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___ADD_TYPE_TO_BOOKING__INT_STRING = IHOTEL_RECEPTIONIST___ADD_TYPE_TO_BOOKING__INT_STRING;

	/**
	 * The operation id for the '<em>Get Check Ins Check Outs For Day</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___GET_CHECK_INS_CHECK_OUTS_FOR_DAY__INT_STRING_STRING = IHOTEL_RECEPTIONIST_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Clear Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CLEAR_BOOKINGS = IHOTEL_RECEPTIONIST_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Filter Available Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___FILTER_AVAILABLE_ROOMS__ELIST_STRING_STRING = IHOTEL_RECEPTIONIST_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Overlapping Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___GET_OVERLAPPING_BOOKINGS__STRING_STRING = IHOTEL_RECEPTIONIST_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Fetch Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___FETCH_BOOKING__INT = IHOTEL_RECEPTIONIST_OPERATION_COUNT + 4;

	/**
	 * The number of operations of the '<em>Booking Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER_OPERATION_COUNT = IHOTEL_RECEPTIONIST_OPERATION_COUNT + 5;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.ITypeAdministrator <em>IType Administrator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.ITypeAdministrator
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getITypeAdministrator()
	 * @generated
	 */
	int ITYPE_ADMINISTRATOR = 10;

	/**
	 * The number of structural features of the '<em>IType Administrator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITYPE_ADMINISTRATOR_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITYPE_ADMINISTRATOR___ADD_ROOM_TYPE__STRING_DOUBLE_INT = 0;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITYPE_ADMINISTRATOR___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT = 1;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITYPE_ADMINISTRATOR___REMOVE_ROOM_TYPE__STRING = 2;

	/**
	 * The number of operations of the '<em>IType Administrator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITYPE_ADMINISTRATOR_OPERATION_COUNT = 3;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.impl.TypeManagerImpl <em>Type Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.impl.TypeManagerImpl
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getTypeManager()
	 * @generated
	 */
	int TYPE_MANAGER = 11;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MANAGER__TYPE = ITYPE_ADMINISTRATOR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Type Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MANAGER_FEATURE_COUNT = ITYPE_ADMINISTRATOR_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT = ITYPE_ADMINISTRATOR___ADD_ROOM_TYPE__STRING_DOUBLE_INT;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MANAGER___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT = ITYPE_ADMINISTRATOR___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MANAGER___REMOVE_ROOM_TYPE__STRING = ITYPE_ADMINISTRATOR___REMOVE_ROOM_TYPE__STRING;

	/**
	 * The operation id for the '<em>Get Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MANAGER___GET_TYPES = ITYPE_ADMINISTRATOR_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Fetch Type By Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MANAGER___FETCH_TYPE_BY_NAME__STRING = ITYPE_ADMINISTRATOR_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Clear Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MANAGER___CLEAR_TYPES = ITYPE_ADMINISTRATOR_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Type Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MANAGER_OPERATION_COUNT = ITYPE_ADMINISTRATOR_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.impl.RoomManagerImpl <em>Room Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.impl.RoomManagerImpl
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getRoomManager()
	 * @generated
	 */
	int ROOM_MANAGER = 13;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.IRoomManager <em>IRoom Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.IRoomManager
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getIRoomManager()
	 * @generated
	 */
	int IROOM_MANAGER = 15;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.IRoomAdministrator <em>IRoom Administrator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.IRoomAdministrator
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getIRoomAdministrator()
	 * @generated
	 */
	int IROOM_ADMINISTRATOR = 14;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.ITypeManager <em>IType Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.ITypeManager
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getITypeManager()
	 * @generated
	 */
	int ITYPE_MANAGER = 12;

	/**
	 * The number of structural features of the '<em>IType Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITYPE_MANAGER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITYPE_MANAGER___GET_TYPES = 0;

	/**
	 * The operation id for the '<em>Fetch Type By Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITYPE_MANAGER___FETCH_TYPE_BY_NAME__STRING = 1;

	/**
	 * The number of operations of the '<em>IType Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITYPE_MANAGER_OPERATION_COUNT = 2;

	/**
	 * The number of structural features of the '<em>IRoom Administrator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ADMINISTRATOR_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Change Room Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ADMINISTRATOR___CHANGE_ROOM_TYPE_OF_ROOM__INT_STRING = 0;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ADMINISTRATOR___ADD_ROOM__INT_STRING = 1;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ADMINISTRATOR___REMOVE_ROOM__INT = 2;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ADMINISTRATOR___BLOCK_ROOM__INT = 3;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ADMINISTRATOR___UNBLOCK_ROOM__INT = 4;

	/**
	 * The number of operations of the '<em>IRoom Administrator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ADMINISTRATOR_OPERATION_COUNT = 5;

	/**
	 * The feature id for the '<em><b>Room</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER__ROOM = IROOM_ADMINISTRATOR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Room Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER_FEATURE_COUNT = IROOM_ADMINISTRATOR_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Change Room Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___CHANGE_ROOM_TYPE_OF_ROOM__INT_STRING = IROOM_ADMINISTRATOR___CHANGE_ROOM_TYPE_OF_ROOM__INT_STRING;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___ADD_ROOM__INT_STRING = IROOM_ADMINISTRATOR___ADD_ROOM__INT_STRING;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___REMOVE_ROOM__INT = IROOM_ADMINISTRATOR___REMOVE_ROOM__INT;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___BLOCK_ROOM__INT = IROOM_ADMINISTRATOR___BLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___UNBLOCK_ROOM__INT = IROOM_ADMINISTRATOR___UNBLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Get Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___GET_ROOMS = IROOM_ADMINISTRATOR_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Unblocked Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___GET_UNBLOCKED_ROOMS__INT = IROOM_ADMINISTRATOR_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___GET_FREE_ROOMS = IROOM_ADMINISTRATOR_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Fetch Room By Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___FETCH_ROOM_BY_ID__INT = IROOM_ADMINISTRATOR_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Clear Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___CLEAR_ROOMS = IROOM_ADMINISTRATOR_OPERATION_COUNT + 4;

	/**
	 * The number of operations of the '<em>Room Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER_OPERATION_COUNT = IROOM_ADMINISTRATOR_OPERATION_COUNT + 5;

	/**
	 * The number of structural features of the '<em>IRoom Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER___GET_ROOMS = 0;

	/**
	 * The operation id for the '<em>Get Unblocked Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER___GET_UNBLOCKED_ROOMS__INT = 1;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER___GET_FREE_ROOMS = 2;

	/**
	 * The operation id for the '<em>Fetch Room By Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER___FETCH_ROOM_BY_ID__INT = 3;

	/**
	 * The number of operations of the '<em>IRoom Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER_OPERATION_COUNT = 4;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.impl.HotelStartupProvidesImpl <em>Hotel Startup Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.impl.HotelStartupProvidesImpl
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getHotelStartupProvides()
	 * @generated
	 */
	int HOTEL_STARTUP_PROVIDES = 16;

	/**
	 * The feature id for the '<em><b>Roommanager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES__ROOMMANAGER = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Bookingmanager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES__BOOKINGMANAGER = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Typemanager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES__TYPEMANAGER = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Hotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES_FEATURE_COUNT = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES___STARTUP__INT = IHOTEL_STARTUP_PROVIDES___STARTUP__INT;

	/**
	 * The number of operations of the '<em>Hotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES_OPERATION_COUNT = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group05.Status <em>Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group05.Status
	 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getStatus()
	 * @generated
	 */
	int STATUS = 17;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides <em>IHotel Customer Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Customer Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides
	 * @generated
	 */
	EClass getIHotelCustomerProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#getFreeRooms(int, java.lang.String, java.lang.String) <em>Get Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#getFreeRooms(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__GetFreeRooms__int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Initiate Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateBooking__String_String_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#addRoomToBooking(java.lang.String, int) <em>Add Room To Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room To Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#addRoomToBooking(java.lang.String, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__AddRoomToBooking__String_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#confirmBooking(int) <em>Confirm Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#confirmBooking(int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__ConfirmBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#initiateCheckout(int) <em>Initiate Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#initiateCheckout(int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateCheckout__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay During Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__PayDuringCheckout__String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#initiateRoomCheckout(int, int) <em>Initiate Room Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Room Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#initiateRoomCheckout(int, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateRoomCheckout__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay Room During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Room During Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__PayRoomDuringCheckout__int_String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#checkInRoom(java.lang.String, int) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides#checkInRoom(java.lang.String, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__CheckInRoom__String_int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group05.FreeRoomTypesDTO <em>Free Room Types DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Free Room Types DTO</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.FreeRoomTypesDTO
	 * @generated
	 */
	EClass getFreeRoomTypesDTO();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.FreeRoomTypesDTO#getRoomTypeDescription <em>Room Type Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Type Description</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.FreeRoomTypesDTO#getRoomTypeDescription()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_RoomTypeDescription();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.FreeRoomTypesDTO#getNumBeds <em>Num Beds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Beds</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.FreeRoomTypesDTO#getNumBeds()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_NumBeds();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.FreeRoomTypesDTO#getPricePerNight <em>Price Per Night</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price Per Night</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.FreeRoomTypesDTO#getPricePerNight()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_PricePerNight();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.FreeRoomTypesDTO#getNumFreeRooms <em>Num Free Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Free Rooms</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.FreeRoomTypesDTO#getNumFreeRooms()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_NumFreeRooms();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group05.IHotelStartupProvides <em>IHotel Startup Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Startup Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelStartupProvides
	 * @generated
	 */
	EClass getIHotelStartupProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelStartupProvides#startup(int) <em>Startup</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Startup</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelStartupProvides#startup(int)
	 * @generated
	 */
	EOperation getIHotelStartupProvides__Startup__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group05.IHotelReceptionist <em>IHotel Receptionist</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Receptionist</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelReceptionist
	 * @generated
	 */
	EClass getIHotelReceptionist();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#checkInBooking(int) <em>Check In Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#checkInBooking(int)
	 * @generated
	 */
	EOperation getIHotelReceptionist__CheckInBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#removeTypeFromBooking(int, java.lang.String) <em>Remove Type From Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Type From Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#removeTypeFromBooking(int, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelReceptionist__RemoveTypeFromBooking__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#setBookingExpectedDates(int, java.lang.String, java.lang.String) <em>Set Booking Expected Dates</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Booking Expected Dates</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#setBookingExpectedDates(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelReceptionist__SetBookingExpectedDates__int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#cancelBooking(int) <em>Cancel Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#cancelBooking(int)
	 * @generated
	 */
	EOperation getIHotelReceptionist__CancelBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#getAllBookings() <em>Get All Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#getAllBookings()
	 * @generated
	 */
	EOperation getIHotelReceptionist__GetAllBookings();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#getOccupiedRooms(java.util.Date) <em>Get Occupied Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Occupied Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#getOccupiedRooms(java.util.Date)
	 * @generated
	 */
	EOperation getIHotelReceptionist__GetOccupiedRooms__Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#getCheckIns(java.lang.String, java.lang.String) <em>Get Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Check Ins</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#getCheckIns(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelReceptionist__GetCheckIns__String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#getCheckOuts(java.lang.String, java.lang.String) <em>Get Check Outs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Check Outs</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#getCheckOuts(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelReceptionist__GetCheckOuts__String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#addCostToRoom(int, int, java.lang.String, double) <em>Add Cost To Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Cost To Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#addCostToRoom(int, int, java.lang.String, double)
	 * @generated
	 */
	EOperation getIHotelReceptionist__AddCostToRoom__int_int_String_double();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#addTypeToBooking(int, java.lang.String) <em>Add Type To Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Type To Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IHotelReceptionist#addTypeToBooking(int, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelReceptionist__AddTypeToBooking__int_String();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group05.Type <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.Type
	 * @generated
	 */
	EClass getType();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.Type#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.Type#getName()
	 * @see #getType()
	 * @generated
	 */
	EAttribute getType_Name();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.Type#getNumberOfBeds <em>Number Of Beds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Of Beds</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.Type#getNumberOfBeds()
	 * @see #getType()
	 * @generated
	 */
	EAttribute getType_NumberOfBeds();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.Type#getPrice <em>Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.Type#getPrice()
	 * @see #getType()
	 * @generated
	 */
	EAttribute getType_Price();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group05.Booking <em>Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booking</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.Booking
	 * @generated
	 */
	EClass getBooking();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group05.Booking#getRoomreservation <em>Roomreservation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Roomreservation</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.Booking#getRoomreservation()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_Roomreservation();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.Booking#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.Booking#getId()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_Id();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.Booking#getDateIn <em>Date In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date In</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.Booking#getDateIn()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_DateIn();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.Booking#getDateOut <em>Date Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date Out</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.Booking#getDateOut()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_DateOut();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.Booking#getFirstName <em>First Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.Booking#getFirstName()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_FirstName();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.Booking#getLastName <em>Last Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.Booking#getLastName()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_LastName();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.Booking#isConfirmed <em>Is Confirmed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Confirmed</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.Booking#isConfirmed()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_IsConfirmed();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.Booking#getTotalPrice() <em>Get Total Price</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Total Price</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.Booking#getTotalPrice()
	 * @generated
	 */
	EOperation getBooking__GetTotalPrice();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.Booking#getStringDateIn() <em>Get String Date In</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get String Date In</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.Booking#getStringDateIn()
	 * @generated
	 */
	EOperation getBooking__GetStringDateIn();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.Booking#getStringDateOut() <em>Get String Date Out</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get String Date Out</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.Booking#getStringDateOut()
	 * @generated
	 */
	EOperation getBooking__GetStringDateOut();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.Booking#addCostToRoom(double, int, java.lang.String) <em>Add Cost To Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Cost To Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.Booking#addCostToRoom(double, int, java.lang.String)
	 * @generated
	 */
	EOperation getBooking__AddCostToRoom__double_int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.Booking#addRoomReservation(se.chalmers.cse.mdsd1617.group05.Type) <em>Add Room Reservation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Reservation</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.Booking#addRoomReservation(se.chalmers.cse.mdsd1617.group05.Type)
	 * @generated
	 */
	EOperation getBooking__AddRoomReservation__Type();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group05.RoomReservation <em>Room Reservation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Reservation</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.RoomReservation
	 * @generated
	 */
	EClass getRoomReservation();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.RoomReservation#getType()
	 * @see #getRoomReservation()
	 * @generated
	 */
	EReference getRoomReservation_Type();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.RoomReservation#getRoom()
	 * @see #getRoomReservation()
	 * @generated
	 */
	EReference getRoomReservation_Room();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getCheckIn <em>Check In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Check In</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.RoomReservation#getCheckIn()
	 * @see #getRoomReservation()
	 * @generated
	 */
	EAttribute getRoomReservation_CheckIn();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getCheckOut <em>Check Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Check Out</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.RoomReservation#getCheckOut()
	 * @see #getRoomReservation()
	 * @generated
	 */
	EAttribute getRoomReservation_CheckOut();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#isPaid <em>Is Paid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Paid</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.RoomReservation#isPaid()
	 * @see #getRoomReservation()
	 * @generated
	 */
	EAttribute getRoomReservation_IsPaid();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getBookingId <em>Booking Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.RoomReservation#getBookingId()
	 * @see #getRoomReservation()
	 * @generated
	 */
	EAttribute getRoomReservation_BookingId();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getExtracost <em>Extracost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Extracost</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.RoomReservation#getExtracost()
	 * @see #getRoomReservation()
	 * @generated
	 */
	EReference getRoomReservation_Extracost();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getPrice() <em>Get Price</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Price</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.RoomReservation#getPrice()
	 * @generated
	 */
	EOperation getRoomReservation__GetPrice();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#addExtraCost(double, java.lang.String) <em>Add Extra Cost</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra Cost</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.RoomReservation#addExtraCost(double, java.lang.String)
	 * @generated
	 */
	EOperation getRoomReservation__AddExtraCost__double_String();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group05.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.Room
	 * @generated
	 */
	EClass getRoom();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group05.Room#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.Room#getType()
	 * @see #getRoom()
	 * @generated
	 */
	EReference getRoom_Type();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.Room#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.Room#getStatus()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_Status();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.Room#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.Room#getNumber()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_Number();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group05.ExtraCost <em>Extra Cost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extra Cost</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.ExtraCost
	 * @generated
	 */
	EClass getExtraCost();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.ExtraCost#getAmount <em>Amount</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Amount</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.ExtraCost#getAmount()
	 * @see #getExtraCost()
	 * @generated
	 */
	EAttribute getExtraCost_Amount();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group05.ExtraCost#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.ExtraCost#getDescription()
	 * @see #getExtraCost()
	 * @generated
	 */
	EAttribute getExtraCost_Description();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group05.BookingManager <em>Booking Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booking Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.BookingManager
	 * @generated
	 */
	EClass getBookingManager();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group05.BookingManager#getBooking <em>Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Booking</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.BookingManager#getBooking()
	 * @see #getBookingManager()
	 * @generated
	 */
	EReference getBookingManager_Booking();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group05.BookingManager#getCurrentCheckoutBooking <em>Current Checkout Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current Checkout Booking</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.BookingManager#getCurrentCheckoutBooking()
	 * @see #getBookingManager()
	 * @generated
	 */
	EReference getBookingManager_CurrentCheckoutBooking();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group05.BookingManager#getInitiatedRoomCheckouts <em>Initiated Room Checkouts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Initiated Room Checkouts</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.BookingManager#getInitiatedRoomCheckouts()
	 * @see #getBookingManager()
	 * @generated
	 */
	EReference getBookingManager_InitiatedRoomCheckouts();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.BookingManager#getCheckInsCheckOutsForDay(int, java.lang.String, java.lang.String) <em>Get Check Ins Check Outs For Day</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Check Ins Check Outs For Day</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.BookingManager#getCheckInsCheckOutsForDay(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getBookingManager__GetCheckInsCheckOutsForDay__int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.BookingManager#clearBookings() <em>Clear Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.BookingManager#clearBookings()
	 * @generated
	 */
	EOperation getBookingManager__ClearBookings();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.BookingManager#filterAvailableRooms(org.eclipse.emf.common.util.EList, java.lang.String, java.lang.String) <em>Filter Available Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Filter Available Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.BookingManager#filterAvailableRooms(org.eclipse.emf.common.util.EList, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getBookingManager__FilterAvailableRooms__EList_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.BookingManager#getOverlappingBookings(java.lang.String, java.lang.String) <em>Get Overlapping Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Overlapping Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.BookingManager#getOverlappingBookings(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getBookingManager__GetOverlappingBookings__String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.BookingManager#fetchBooking(int) <em>Fetch Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Fetch Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.BookingManager#fetchBooking(int)
	 * @generated
	 */
	EOperation getBookingManager__FetchBooking__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group05.ITypeAdministrator <em>IType Administrator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IType Administrator</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.ITypeAdministrator
	 * @generated
	 */
	EClass getITypeAdministrator();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.ITypeAdministrator#addRoomType(java.lang.String, double, int) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.ITypeAdministrator#addRoomType(java.lang.String, double, int)
	 * @generated
	 */
	EOperation getITypeAdministrator__AddRoomType__String_double_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.ITypeAdministrator#updateRoomType(java.lang.String, java.lang.String, double, int) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.ITypeAdministrator#updateRoomType(java.lang.String, java.lang.String, double, int)
	 * @generated
	 */
	EOperation getITypeAdministrator__UpdateRoomType__String_String_double_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.ITypeAdministrator#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.ITypeAdministrator#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getITypeAdministrator__RemoveRoomType__String();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group05.TypeManager <em>Type Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.TypeManager
	 * @generated
	 */
	EClass getTypeManager();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group05.TypeManager#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.TypeManager#getType()
	 * @see #getTypeManager()
	 * @generated
	 */
	EReference getTypeManager_Type();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.TypeManager#clearTypes() <em>Clear Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear Types</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.TypeManager#clearTypes()
	 * @generated
	 */
	EOperation getTypeManager__ClearTypes();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group05.RoomManager <em>Room Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.RoomManager
	 * @generated
	 */
	EClass getRoomManager();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group05.RoomManager#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Room</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.RoomManager#getRoom()
	 * @see #getRoomManager()
	 * @generated
	 */
	EReference getRoomManager_Room();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.RoomManager#clearRooms() <em>Clear Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.RoomManager#clearRooms()
	 * @generated
	 */
	EOperation getRoomManager__ClearRooms();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group05.IRoomManager <em>IRoom Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.IRoomManager
	 * @generated
	 */
	EClass getIRoomManager();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IRoomManager#getRooms() <em>Get Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IRoomManager#getRooms()
	 * @generated
	 */
	EOperation getIRoomManager__GetRooms();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IRoomManager#getUnblockedRooms(int) <em>Get Unblocked Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Unblocked Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IRoomManager#getUnblockedRooms(int)
	 * @generated
	 */
	EOperation getIRoomManager__GetUnblockedRooms__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IRoomManager#getFreeRooms() <em>Get Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IRoomManager#getFreeRooms()
	 * @generated
	 */
	EOperation getIRoomManager__GetFreeRooms();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IRoomManager#fetchRoomById(int) <em>Fetch Room By Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Fetch Room By Id</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IRoomManager#fetchRoomById(int)
	 * @generated
	 */
	EOperation getIRoomManager__FetchRoomById__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group05.IRoomAdministrator <em>IRoom Administrator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Administrator</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.IRoomAdministrator
	 * @generated
	 */
	EClass getIRoomAdministrator();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IRoomAdministrator#changeRoomTypeOfRoom(int, java.lang.String) <em>Change Room Type Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type Of Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IRoomAdministrator#changeRoomTypeOfRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getIRoomAdministrator__ChangeRoomTypeOfRoom__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IRoomAdministrator#addRoom(int, java.lang.String) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IRoomAdministrator#addRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getIRoomAdministrator__AddRoom__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IRoomAdministrator#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IRoomAdministrator#removeRoom(int)
	 * @generated
	 */
	EOperation getIRoomAdministrator__RemoveRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IRoomAdministrator#blockRoom(int) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IRoomAdministrator#blockRoom(int)
	 * @generated
	 */
	EOperation getIRoomAdministrator__BlockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.IRoomAdministrator#unblockRoom(int) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.IRoomAdministrator#unblockRoom(int)
	 * @generated
	 */
	EOperation getIRoomAdministrator__UnblockRoom__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group05.ITypeManager <em>IType Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IType Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.ITypeManager
	 * @generated
	 */
	EClass getITypeManager();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.ITypeManager#getTypes() <em>Get Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Types</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.ITypeManager#getTypes()
	 * @generated
	 */
	EOperation getITypeManager__GetTypes();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group05.ITypeManager#fetchTypeByName(java.lang.String) <em>Fetch Type By Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Fetch Type By Name</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group05.ITypeManager#fetchTypeByName(java.lang.String)
	 * @generated
	 */
	EOperation getITypeManager__FetchTypeByName__String();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group05.HotelStartupProvides <em>Hotel Startup Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Startup Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.HotelStartupProvides
	 * @generated
	 */
	EClass getHotelStartupProvides();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group05.HotelStartupProvides#getRoommanager <em>Roommanager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Roommanager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.HotelStartupProvides#getRoommanager()
	 * @see #getHotelStartupProvides()
	 * @generated
	 */
	EReference getHotelStartupProvides_Roommanager();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group05.HotelStartupProvides#getBookingmanager <em>Bookingmanager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Bookingmanager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.HotelStartupProvides#getBookingmanager()
	 * @see #getHotelStartupProvides()
	 * @generated
	 */
	EReference getHotelStartupProvides_Bookingmanager();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group05.HotelStartupProvides#getTypemanager <em>Typemanager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Typemanager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.HotelStartupProvides#getTypemanager()
	 * @see #getHotelStartupProvides()
	 * @generated
	 */
	EReference getHotelStartupProvides_Typemanager();

	/**
	 * Returns the meta object for enum '{@link se.chalmers.cse.mdsd1617.group05.Status <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group05.Status
	 * @generated
	 */
	EEnum getStatus();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Group05Factory getGroup05Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides <em>IHotel Customer Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.IHotelCustomerProvides
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getIHotelCustomerProvides()
		 * @generated
		 */
		EClass IHOTEL_CUSTOMER_PROVIDES = eINSTANCE.getIHotelCustomerProvides();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__GetFreeRooms__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Initiate Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__InitiateBooking__String_String_String_String();

		/**
		 * The meta object literal for the '<em><b>Add Room To Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = eINSTANCE.getIHotelCustomerProvides__AddRoomToBooking__String_int();

		/**
		 * The meta object literal for the '<em><b>Confirm Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = eINSTANCE.getIHotelCustomerProvides__ConfirmBooking__int();

		/**
		 * The meta object literal for the '<em><b>Initiate Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = eINSTANCE.getIHotelCustomerProvides__InitiateCheckout__int();

		/**
		 * The meta object literal for the '<em><b>Pay During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__PayDuringCheckout__String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Initiate Room Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = eINSTANCE.getIHotelCustomerProvides__InitiateRoomCheckout__int_int();

		/**
		 * The meta object literal for the '<em><b>Pay Room During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__PayRoomDuringCheckout__int_String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = eINSTANCE.getIHotelCustomerProvides__CheckInRoom__String_int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.impl.FreeRoomTypesDTOImpl <em>Free Room Types DTO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.impl.FreeRoomTypesDTOImpl
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getFreeRoomTypesDTO()
		 * @generated
		 */
		EClass FREE_ROOM_TYPES_DTO = eINSTANCE.getFreeRoomTypesDTO();

		/**
		 * The meta object literal for the '<em><b>Room Type Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__ROOM_TYPE_DESCRIPTION = eINSTANCE.getFreeRoomTypesDTO_RoomTypeDescription();

		/**
		 * The meta object literal for the '<em><b>Num Beds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__NUM_BEDS = eINSTANCE.getFreeRoomTypesDTO_NumBeds();

		/**
		 * The meta object literal for the '<em><b>Price Per Night</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__PRICE_PER_NIGHT = eINSTANCE.getFreeRoomTypesDTO_PricePerNight();

		/**
		 * The meta object literal for the '<em><b>Num Free Rooms</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__NUM_FREE_ROOMS = eINSTANCE.getFreeRoomTypesDTO_NumFreeRooms();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelStartupProvides <em>IHotel Startup Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.IHotelStartupProvides
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getIHotelStartupProvides()
		 * @generated
		 */
		EClass IHOTEL_STARTUP_PROVIDES = eINSTANCE.getIHotelStartupProvides();

		/**
		 * The meta object literal for the '<em><b>Startup</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_STARTUP_PROVIDES___STARTUP__INT = eINSTANCE.getIHotelStartupProvides__Startup__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.IHotelReceptionist <em>IHotel Receptionist</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.IHotelReceptionist
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getIHotelReceptionist()
		 * @generated
		 */
		EClass IHOTEL_RECEPTIONIST = eINSTANCE.getIHotelReceptionist();

		/**
		 * The meta object literal for the '<em><b>Check In Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST___CHECK_IN_BOOKING__INT = eINSTANCE.getIHotelReceptionist__CheckInBooking__int();

		/**
		 * The meta object literal for the '<em><b>Remove Type From Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST___REMOVE_TYPE_FROM_BOOKING__INT_STRING = eINSTANCE.getIHotelReceptionist__RemoveTypeFromBooking__int_String();

		/**
		 * The meta object literal for the '<em><b>Set Booking Expected Dates</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST___SET_BOOKING_EXPECTED_DATES__INT_STRING_STRING = eINSTANCE.getIHotelReceptionist__SetBookingExpectedDates__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST___CANCEL_BOOKING__INT = eINSTANCE.getIHotelReceptionist__CancelBooking__int();

		/**
		 * The meta object literal for the '<em><b>Get All Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST___GET_ALL_BOOKINGS = eINSTANCE.getIHotelReceptionist__GetAllBookings();

		/**
		 * The meta object literal for the '<em><b>Get Occupied Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST___GET_OCCUPIED_ROOMS__DATE = eINSTANCE.getIHotelReceptionist__GetOccupiedRooms__Date();

		/**
		 * The meta object literal for the '<em><b>Get Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST___GET_CHECK_INS__STRING_STRING = eINSTANCE.getIHotelReceptionist__GetCheckIns__String_String();

		/**
		 * The meta object literal for the '<em><b>Get Check Outs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST___GET_CHECK_OUTS__STRING_STRING = eINSTANCE.getIHotelReceptionist__GetCheckOuts__String_String();

		/**
		 * The meta object literal for the '<em><b>Add Cost To Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST___ADD_COST_TO_ROOM__INT_INT_STRING_DOUBLE = eINSTANCE.getIHotelReceptionist__AddCostToRoom__int_int_String_double();

		/**
		 * The meta object literal for the '<em><b>Add Type To Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST___ADD_TYPE_TO_BOOKING__INT_STRING = eINSTANCE.getIHotelReceptionist__AddTypeToBooking__int_String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.impl.TypeImpl <em>Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.impl.TypeImpl
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getType()
		 * @generated
		 */
		EClass TYPE = eINSTANCE.getType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE__NAME = eINSTANCE.getType_Name();

		/**
		 * The meta object literal for the '<em><b>Number Of Beds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE__NUMBER_OF_BEDS = eINSTANCE.getType_NumberOfBeds();

		/**
		 * The meta object literal for the '<em><b>Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE__PRICE = eINSTANCE.getType_Price();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.impl.BookingImpl <em>Booking</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.impl.BookingImpl
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getBooking()
		 * @generated
		 */
		EClass BOOKING = eINSTANCE.getBooking();

		/**
		 * The meta object literal for the '<em><b>Roomreservation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__ROOMRESERVATION = eINSTANCE.getBooking_Roomreservation();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__ID = eINSTANCE.getBooking_Id();

		/**
		 * The meta object literal for the '<em><b>Date In</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__DATE_IN = eINSTANCE.getBooking_DateIn();

		/**
		 * The meta object literal for the '<em><b>Date Out</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__DATE_OUT = eINSTANCE.getBooking_DateOut();

		/**
		 * The meta object literal for the '<em><b>First Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__FIRST_NAME = eINSTANCE.getBooking_FirstName();

		/**
		 * The meta object literal for the '<em><b>Last Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__LAST_NAME = eINSTANCE.getBooking_LastName();

		/**
		 * The meta object literal for the '<em><b>Is Confirmed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__IS_CONFIRMED = eINSTANCE.getBooking_IsConfirmed();

		/**
		 * The meta object literal for the '<em><b>Get Total Price</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___GET_TOTAL_PRICE = eINSTANCE.getBooking__GetTotalPrice();

		/**
		 * The meta object literal for the '<em><b>Get String Date In</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___GET_STRING_DATE_IN = eINSTANCE.getBooking__GetStringDateIn();

		/**
		 * The meta object literal for the '<em><b>Get String Date Out</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___GET_STRING_DATE_OUT = eINSTANCE.getBooking__GetStringDateOut();

		/**
		 * The meta object literal for the '<em><b>Add Cost To Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___ADD_COST_TO_ROOM__DOUBLE_INT_STRING = eINSTANCE.getBooking__AddCostToRoom__double_int_String();

		/**
		 * The meta object literal for the '<em><b>Add Room Reservation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___ADD_ROOM_RESERVATION__TYPE = eINSTANCE.getBooking__AddRoomReservation__Type();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.impl.RoomReservationImpl <em>Room Reservation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.impl.RoomReservationImpl
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getRoomReservation()
		 * @generated
		 */
		EClass ROOM_RESERVATION = eINSTANCE.getRoomReservation();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_RESERVATION__TYPE = eINSTANCE.getRoomReservation_Type();

		/**
		 * The meta object literal for the '<em><b>Room</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_RESERVATION__ROOM = eINSTANCE.getRoomReservation_Room();

		/**
		 * The meta object literal for the '<em><b>Check In</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_RESERVATION__CHECK_IN = eINSTANCE.getRoomReservation_CheckIn();

		/**
		 * The meta object literal for the '<em><b>Check Out</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_RESERVATION__CHECK_OUT = eINSTANCE.getRoomReservation_CheckOut();

		/**
		 * The meta object literal for the '<em><b>Is Paid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_RESERVATION__IS_PAID = eINSTANCE.getRoomReservation_IsPaid();

		/**
		 * The meta object literal for the '<em><b>Booking Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_RESERVATION__BOOKING_ID = eINSTANCE.getRoomReservation_BookingId();

		/**
		 * The meta object literal for the '<em><b>Extracost</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_RESERVATION__EXTRACOST = eINSTANCE.getRoomReservation_Extracost();

		/**
		 * The meta object literal for the '<em><b>Get Price</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_RESERVATION___GET_PRICE = eINSTANCE.getRoomReservation__GetPrice();

		/**
		 * The meta object literal for the '<em><b>Add Extra Cost</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_RESERVATION___ADD_EXTRA_COST__DOUBLE_STRING = eINSTANCE.getRoomReservation__AddExtraCost__double_String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.impl.RoomImpl <em>Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.impl.RoomImpl
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getRoom()
		 * @generated
		 */
		EClass ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM__TYPE = eINSTANCE.getRoom_Type();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__STATUS = eINSTANCE.getRoom_Status();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__NUMBER = eINSTANCE.getRoom_Number();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.impl.ExtraCostImpl <em>Extra Cost</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.impl.ExtraCostImpl
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getExtraCost()
		 * @generated
		 */
		EClass EXTRA_COST = eINSTANCE.getExtraCost();

		/**
		 * The meta object literal for the '<em><b>Amount</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRA_COST__AMOUNT = eINSTANCE.getExtraCost_Amount();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRA_COST__DESCRIPTION = eINSTANCE.getExtraCost_Description();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.impl.BookingManagerImpl <em>Booking Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.impl.BookingManagerImpl
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getBookingManager()
		 * @generated
		 */
		EClass BOOKING_MANAGER = eINSTANCE.getBookingManager();

		/**
		 * The meta object literal for the '<em><b>Booking</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_MANAGER__BOOKING = eINSTANCE.getBookingManager_Booking();

		/**
		 * The meta object literal for the '<em><b>Current Checkout Booking</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_MANAGER__CURRENT_CHECKOUT_BOOKING = eINSTANCE.getBookingManager_CurrentCheckoutBooking();

		/**
		 * The meta object literal for the '<em><b>Initiated Room Checkouts</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_MANAGER__INITIATED_ROOM_CHECKOUTS = eINSTANCE.getBookingManager_InitiatedRoomCheckouts();

		/**
		 * The meta object literal for the '<em><b>Get Check Ins Check Outs For Day</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_MANAGER___GET_CHECK_INS_CHECK_OUTS_FOR_DAY__INT_STRING_STRING = eINSTANCE.getBookingManager__GetCheckInsCheckOutsForDay__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Clear Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_MANAGER___CLEAR_BOOKINGS = eINSTANCE.getBookingManager__ClearBookings();

		/**
		 * The meta object literal for the '<em><b>Filter Available Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_MANAGER___FILTER_AVAILABLE_ROOMS__ELIST_STRING_STRING = eINSTANCE.getBookingManager__FilterAvailableRooms__EList_String_String();

		/**
		 * The meta object literal for the '<em><b>Get Overlapping Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_MANAGER___GET_OVERLAPPING_BOOKINGS__STRING_STRING = eINSTANCE.getBookingManager__GetOverlappingBookings__String_String();

		/**
		 * The meta object literal for the '<em><b>Fetch Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING_MANAGER___FETCH_BOOKING__INT = eINSTANCE.getBookingManager__FetchBooking__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.ITypeAdministrator <em>IType Administrator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.ITypeAdministrator
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getITypeAdministrator()
		 * @generated
		 */
		EClass ITYPE_ADMINISTRATOR = eINSTANCE.getITypeAdministrator();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ITYPE_ADMINISTRATOR___ADD_ROOM_TYPE__STRING_DOUBLE_INT = eINSTANCE.getITypeAdministrator__AddRoomType__String_double_int();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ITYPE_ADMINISTRATOR___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT = eINSTANCE.getITypeAdministrator__UpdateRoomType__String_String_double_int();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ITYPE_ADMINISTRATOR___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getITypeAdministrator__RemoveRoomType__String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.impl.TypeManagerImpl <em>Type Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.impl.TypeManagerImpl
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getTypeManager()
		 * @generated
		 */
		EClass TYPE_MANAGER = eINSTANCE.getTypeManager();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPE_MANAGER__TYPE = eINSTANCE.getTypeManager_Type();

		/**
		 * The meta object literal for the '<em><b>Clear Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TYPE_MANAGER___CLEAR_TYPES = eINSTANCE.getTypeManager__ClearTypes();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.impl.RoomManagerImpl <em>Room Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.impl.RoomManagerImpl
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getRoomManager()
		 * @generated
		 */
		EClass ROOM_MANAGER = eINSTANCE.getRoomManager();

		/**
		 * The meta object literal for the '<em><b>Room</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_MANAGER__ROOM = eINSTANCE.getRoomManager_Room();

		/**
		 * The meta object literal for the '<em><b>Clear Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_MANAGER___CLEAR_ROOMS = eINSTANCE.getRoomManager__ClearRooms();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.IRoomManager <em>IRoom Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.IRoomManager
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getIRoomManager()
		 * @generated
		 */
		EClass IROOM_MANAGER = eINSTANCE.getIRoomManager();

		/**
		 * The meta object literal for the '<em><b>Get Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER___GET_ROOMS = eINSTANCE.getIRoomManager__GetRooms();

		/**
		 * The meta object literal for the '<em><b>Get Unblocked Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER___GET_UNBLOCKED_ROOMS__INT = eINSTANCE.getIRoomManager__GetUnblockedRooms__int();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER___GET_FREE_ROOMS = eINSTANCE.getIRoomManager__GetFreeRooms();

		/**
		 * The meta object literal for the '<em><b>Fetch Room By Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER___FETCH_ROOM_BY_ID__INT = eINSTANCE.getIRoomManager__FetchRoomById__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.IRoomAdministrator <em>IRoom Administrator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.IRoomAdministrator
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getIRoomAdministrator()
		 * @generated
		 */
		EClass IROOM_ADMINISTRATOR = eINSTANCE.getIRoomAdministrator();

		/**
		 * The meta object literal for the '<em><b>Change Room Type Of Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_ADMINISTRATOR___CHANGE_ROOM_TYPE_OF_ROOM__INT_STRING = eINSTANCE.getIRoomAdministrator__ChangeRoomTypeOfRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_ADMINISTRATOR___ADD_ROOM__INT_STRING = eINSTANCE.getIRoomAdministrator__AddRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_ADMINISTRATOR___REMOVE_ROOM__INT = eINSTANCE.getIRoomAdministrator__RemoveRoom__int();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_ADMINISTRATOR___BLOCK_ROOM__INT = eINSTANCE.getIRoomAdministrator__BlockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_ADMINISTRATOR___UNBLOCK_ROOM__INT = eINSTANCE.getIRoomAdministrator__UnblockRoom__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.ITypeManager <em>IType Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.ITypeManager
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getITypeManager()
		 * @generated
		 */
		EClass ITYPE_MANAGER = eINSTANCE.getITypeManager();

		/**
		 * The meta object literal for the '<em><b>Get Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ITYPE_MANAGER___GET_TYPES = eINSTANCE.getITypeManager__GetTypes();

		/**
		 * The meta object literal for the '<em><b>Fetch Type By Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ITYPE_MANAGER___FETCH_TYPE_BY_NAME__STRING = eINSTANCE.getITypeManager__FetchTypeByName__String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.impl.HotelStartupProvidesImpl <em>Hotel Startup Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.impl.HotelStartupProvidesImpl
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getHotelStartupProvides()
		 * @generated
		 */
		EClass HOTEL_STARTUP_PROVIDES = eINSTANCE.getHotelStartupProvides();

		/**
		 * The meta object literal for the '<em><b>Roommanager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_STARTUP_PROVIDES__ROOMMANAGER = eINSTANCE.getHotelStartupProvides_Roommanager();

		/**
		 * The meta object literal for the '<em><b>Bookingmanager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_STARTUP_PROVIDES__BOOKINGMANAGER = eINSTANCE.getHotelStartupProvides_Bookingmanager();

		/**
		 * The meta object literal for the '<em><b>Typemanager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_STARTUP_PROVIDES__TYPEMANAGER = eINSTANCE.getHotelStartupProvides_Typemanager();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group05.Status <em>Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group05.Status
		 * @see se.chalmers.cse.mdsd1617.group05.impl.Group05PackageImpl#getStatus()
		 * @generated
		 */
		EEnum STATUS = eINSTANCE.getStatus();

	}

} //Group05Package
