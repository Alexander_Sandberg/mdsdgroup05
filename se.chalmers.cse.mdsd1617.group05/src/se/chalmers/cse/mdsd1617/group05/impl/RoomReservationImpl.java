/**
 */
package se.chalmers.cse.mdsd1617.group05.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Date;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group05.ExtraCost;
import se.chalmers.cse.mdsd1617.group05.Group05Factory;
import se.chalmers.cse.mdsd1617.group05.Group05Package;
import se.chalmers.cse.mdsd1617.group05.Room;
import se.chalmers.cse.mdsd1617.group05.RoomReservation;
import se.chalmers.cse.mdsd1617.group05.Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Room Reservation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.RoomReservationImpl#getType <em>Type</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.RoomReservationImpl#getRoom <em>Room</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.RoomReservationImpl#getCheckIn <em>Check In</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.RoomReservationImpl#getCheckOut <em>Check Out</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.RoomReservationImpl#isPaid <em>Is Paid</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.RoomReservationImpl#getBookingId <em>Booking Id</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.RoomReservationImpl#getExtracost <em>Extracost</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomReservationImpl extends MinimalEObjectImpl.Container implements RoomReservation {
	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected Type type;

	/**
	 * The cached value of the '{@link #getRoom() <em>Room</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoom()
	 * @generated
	 * @ordered
	 */
	protected Room room;

	/**
	 * The default value of the '{@link #getCheckIn() <em>Check In</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckIn()
	 * @generated
	 * @ordered
	 */
	protected static final Date CHECK_IN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCheckIn() <em>Check In</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckIn()
	 * @generated
	 * @ordered
	 */
	protected Date checkIn = CHECK_IN_EDEFAULT;

	/**
	 * The default value of the '{@link #getCheckOut() <em>Check Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckOut()
	 * @generated
	 * @ordered
	 */
	protected static final Date CHECK_OUT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCheckOut() <em>Check Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckOut()
	 * @generated
	 * @ordered
	 */
	protected Date checkOut = CHECK_OUT_EDEFAULT;

	/**
	 * The default value of the '{@link #isPaid() <em>Is Paid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPaid()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_PAID_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPaid() <em>Is Paid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPaid()
	 * @generated
	 * @ordered
	 */
	protected boolean isPaid = IS_PAID_EDEFAULT;

	/**
	 * The default value of the '{@link #getBookingId() <em>Booking Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingId()
	 * @generated
	 * @ordered
	 */
	protected static final int BOOKING_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBookingId() <em>Booking Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingId()
	 * @generated
	 * @ordered
	 */
	protected int bookingId = BOOKING_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getExtracost() <em>Extracost</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtracost()
	 * @generated
	 * @ordered
	 */
	protected ExtraCost extracost;

	protected Group05Factory factory = Group05FactoryImpl.init();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomReservationImpl() {
		super();
	}
	 

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group05Package.Literals.ROOM_RESERVATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (Type)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group05Package.ROOM_RESERVATION__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(Type newType) {
		Type oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group05Package.ROOM_RESERVATION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room getRoom() {
		if (room != null && room.eIsProxy()) {
			InternalEObject oldRoom = (InternalEObject)room;
			room = (Room)eResolveProxy(oldRoom);
			if (room != oldRoom) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group05Package.ROOM_RESERVATION__ROOM, oldRoom, room));
			}
		}
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room basicGetRoom() {
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoom(Room newRoom) {
		Room oldRoom = room;
		room = newRoom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group05Package.ROOM_RESERVATION__ROOM, oldRoom, room));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getCheckIn() {
		return checkIn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckIn(Date newCheckIn) {
		Date oldCheckIn = checkIn;
		checkIn = newCheckIn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group05Package.ROOM_RESERVATION__CHECK_IN, oldCheckIn, checkIn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getCheckOut() {
		return checkOut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckOut(Date newCheckOut) {
		Date oldCheckOut = checkOut;
		checkOut = newCheckOut;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group05Package.ROOM_RESERVATION__CHECK_OUT, oldCheckOut, checkOut));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPaid() {
		return isPaid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsPaid(boolean newIsPaid) {
		boolean oldIsPaid = isPaid;
		isPaid = newIsPaid;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group05Package.ROOM_RESERVATION__IS_PAID, oldIsPaid, isPaid));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBookingId() {
		return bookingId;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingId(int newBookingId) {
		int oldBookingId = bookingId;
		bookingId = newBookingId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group05Package.ROOM_RESERVATION__BOOKING_ID, oldBookingId, bookingId));
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtraCost getExtracost() {
		if (extracost != null && extracost.eIsProxy()) {
			InternalEObject oldExtracost = (InternalEObject)extracost;
			extracost = (ExtraCost)eResolveProxy(oldExtracost);
			if (extracost != oldExtracost) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group05Package.ROOM_RESERVATION__EXTRACOST, oldExtracost, extracost));
			}
		}
		return extracost;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtraCost basicGetExtracost() {
		return extracost;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtracost(ExtraCost newExtracost) {
		ExtraCost oldExtracost = extracost;
		extracost = newExtracost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group05Package.ROOM_RESERVATION__EXTRACOST, oldExtracost, extracost));
	}


	/**
	 * <!-- begin-user-doc -->
	 * 
	 * @author Johan - Marco Trifance
	 * @purpose Returns the total price for this RoomReservationRes. Total
	 * 			price is equal to the base price (or "type-price") plus the amount
	 * 			of the ExtraCost if any.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double getPrice() {
		if(extracost != null)
			return type.getPrice() + extracost.getAmount();
		else
			return type.getPrice();
	}

	/**
	 * <!-- begin-user-doc -->
	 * @author		Marco Trifance
	 * @purpose		Sets the 'extraCost' to a new instance with provided 
	 * 				'amount' and 'description'.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addExtraCost(double amount, String description) {
		ExtraCost ec = factory.createExtraCost();
		ec.setAmount(amount);
		ec.setDescription(description);
		setExtracost(ec);
		return true;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group05Package.ROOM_RESERVATION__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case Group05Package.ROOM_RESERVATION__ROOM:
				if (resolve) return getRoom();
				return basicGetRoom();
			case Group05Package.ROOM_RESERVATION__CHECK_IN:
				return getCheckIn();
			case Group05Package.ROOM_RESERVATION__CHECK_OUT:
				return getCheckOut();
			case Group05Package.ROOM_RESERVATION__IS_PAID:
				return isPaid();
			case Group05Package.ROOM_RESERVATION__BOOKING_ID:
				return getBookingId();
			case Group05Package.ROOM_RESERVATION__EXTRACOST:
				if (resolve) return getExtracost();
				return basicGetExtracost();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group05Package.ROOM_RESERVATION__TYPE:
				setType((Type)newValue);
				return;
			case Group05Package.ROOM_RESERVATION__ROOM:
				setRoom((Room)newValue);
				return;
			case Group05Package.ROOM_RESERVATION__CHECK_IN:
				setCheckIn((Date)newValue);
				return;
			case Group05Package.ROOM_RESERVATION__CHECK_OUT:
				setCheckOut((Date)newValue);
				return;
			case Group05Package.ROOM_RESERVATION__IS_PAID:
				setIsPaid((Boolean)newValue);
				return;
			case Group05Package.ROOM_RESERVATION__BOOKING_ID:
				setBookingId((Integer)newValue);
				return;
			case Group05Package.ROOM_RESERVATION__EXTRACOST:
				setExtracost((ExtraCost)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group05Package.ROOM_RESERVATION__TYPE:
				setType((Type)null);
				return;
			case Group05Package.ROOM_RESERVATION__ROOM:
				setRoom((Room)null);
				return;
			case Group05Package.ROOM_RESERVATION__CHECK_IN:
				setCheckIn(CHECK_IN_EDEFAULT);
				return;
			case Group05Package.ROOM_RESERVATION__CHECK_OUT:
				setCheckOut(CHECK_OUT_EDEFAULT);
				return;
			case Group05Package.ROOM_RESERVATION__IS_PAID:
				setIsPaid(IS_PAID_EDEFAULT);
				return;
			case Group05Package.ROOM_RESERVATION__BOOKING_ID:
				setBookingId(BOOKING_ID_EDEFAULT);
				return;
			case Group05Package.ROOM_RESERVATION__EXTRACOST:
				setExtracost((ExtraCost)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group05Package.ROOM_RESERVATION__TYPE:
				return type != null;
			case Group05Package.ROOM_RESERVATION__ROOM:
				return room != null;
			case Group05Package.ROOM_RESERVATION__CHECK_IN:
				return CHECK_IN_EDEFAULT == null ? checkIn != null : !CHECK_IN_EDEFAULT.equals(checkIn);
			case Group05Package.ROOM_RESERVATION__CHECK_OUT:
				return CHECK_OUT_EDEFAULT == null ? checkOut != null : !CHECK_OUT_EDEFAULT.equals(checkOut);
			case Group05Package.ROOM_RESERVATION__IS_PAID:
				return isPaid != IS_PAID_EDEFAULT;
			case Group05Package.ROOM_RESERVATION__BOOKING_ID:
				return bookingId != BOOKING_ID_EDEFAULT;
			case Group05Package.ROOM_RESERVATION__EXTRACOST:
				return extracost != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group05Package.ROOM_RESERVATION___GET_PRICE:
				return getPrice();
			case Group05Package.ROOM_RESERVATION___ADD_EXTRA_COST__DOUBLE_STRING:
				return addExtraCost((Double)arguments.get(0), (String)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checkIn: ");
		result.append(checkIn);
		result.append(", checkOut: ");
		result.append(checkOut);
		result.append(", isPaid: ");
		result.append(isPaid);
		result.append(", bookingId: ");
		result.append(bookingId);
		result.append(')');
		return result.toString();
	}

} //RoomReservationImpl
