/**
 */
package se.chalmers.cse.mdsd1617.group05.impl;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group05.BookingManager;
import se.chalmers.cse.mdsd1617.group05.Group05Factory;
import se.chalmers.cse.mdsd1617.group05.Group05Package;
import se.chalmers.cse.mdsd1617.group05.HotelStartupProvides;
import se.chalmers.cse.mdsd1617.group05.RoomManager;
import se.chalmers.cse.mdsd1617.group05.TypeManager;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.HotelStartupProvidesImpl#getRoommanager <em>Roommanager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.HotelStartupProvidesImpl#getBookingmanager <em>Bookingmanager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.HotelStartupProvidesImpl#getTypemanager <em>Typemanager</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelStartupProvidesImpl extends MinimalEObjectImpl.Container implements HotelStartupProvides {
	/**
	 * The cached value of the '{@link #getRoommanager() <em>Roommanager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoommanager()
	 * @generated
	 * @ordered
	 */
	protected RoomManager roommanager;
	
	private static HotelStartupProvidesImpl instance = null;

	/**
	 * The cached value of the '{@link #getBookingmanager() <em>Bookingmanager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingmanager()
	 * @generated
	 * @ordered
	 */
	protected BookingManager bookingmanager;

	/**
	 * The cached value of the '{@link #getTypemanager() <em>Typemanager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypemanager()
	 * @generated
	 * @ordered
	 */
	protected TypeManager typemanager;
	
	
	protected Group05Factory factory = Group05FactoryImpl.init();
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	private HotelStartupProvidesImpl() {
		super();
	}
	
	public static HotelStartupProvidesImpl getInstance(){
		if (instance == null){
			instance = new HotelStartupProvidesImpl();
		}
		return instance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group05Package.Literals.HOTEL_STARTUP_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomManager getRoommanager() {
		if (roommanager != null && roommanager.eIsProxy()) {
			InternalEObject oldRoommanager = (InternalEObject)roommanager;
			roommanager = (RoomManager)eResolveProxy(oldRoommanager);
			if (roommanager != oldRoommanager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group05Package.HOTEL_STARTUP_PROVIDES__ROOMMANAGER, oldRoommanager, roommanager));
			}
		}
		return roommanager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomManager basicGetRoommanager() {
		return roommanager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoommanager(RoomManager newRoommanager) {
		RoomManager oldRoommanager = roommanager;
		roommanager = newRoommanager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group05Package.HOTEL_STARTUP_PROVIDES__ROOMMANAGER, oldRoommanager, roommanager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingManager getBookingmanager() {
		if (bookingmanager != null && bookingmanager.eIsProxy()) {
			InternalEObject oldBookingmanager = (InternalEObject)bookingmanager;
			bookingmanager = (BookingManager)eResolveProxy(oldBookingmanager);
			if (bookingmanager != oldBookingmanager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group05Package.HOTEL_STARTUP_PROVIDES__BOOKINGMANAGER, oldBookingmanager, bookingmanager));
			}
		}
		return bookingmanager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingManager basicGetBookingmanager() {
		return bookingmanager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingmanager(BookingManager newBookingmanager) {
		BookingManager oldBookingmanager = bookingmanager;
		bookingmanager = newBookingmanager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group05Package.HOTEL_STARTUP_PROVIDES__BOOKINGMANAGER, oldBookingmanager, bookingmanager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeManager getTypemanager() {
		if (typemanager != null && typemanager.eIsProxy()) {
			InternalEObject oldTypemanager = (InternalEObject)typemanager;
			typemanager = (TypeManager)eResolveProxy(oldTypemanager);
			if (typemanager != oldTypemanager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group05Package.HOTEL_STARTUP_PROVIDES__TYPEMANAGER, oldTypemanager, typemanager));
			}
		}
		return typemanager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeManager basicGetTypemanager() {
		return typemanager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypemanager(TypeManager newTypemanager) {
		TypeManager oldTypemanager = typemanager;
		typemanager = newTypemanager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group05Package.HOTEL_STARTUP_PROVIDES__TYPEMANAGER, oldTypemanager, typemanager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Isaac Gonzalez
	 * @purpose startup(int numRooms) basically starts up the system.
	 * It instantiate all the managers and then clears them.
	 * Lastly it creates a default Type and adds numRooms of that type to
	 * the RoomManager.
	 * @generated NOT
	 */
	public void startup(int numRooms) {
		
		setBookingmanager(factory.createBookingManager());
		setRoommanager(factory.createRoomManager());
		setTypemanager(factory.createTypeManager());
		
		this.bookingmanager.clearBookings();
		this.roommanager.clearRooms();
		this.typemanager.clearTypes();
		//defaults, typename = "default", price = 30, numofbeds = 2
		typemanager.addRoomType("default", 30, 2);
		
		for (int i = 1; i <= numRooms; i++) {
			roommanager.addRoom(i, "default");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group05Package.HOTEL_STARTUP_PROVIDES__ROOMMANAGER:
				if (resolve) return getRoommanager();
				return basicGetRoommanager();
			case Group05Package.HOTEL_STARTUP_PROVIDES__BOOKINGMANAGER:
				if (resolve) return getBookingmanager();
				return basicGetBookingmanager();
			case Group05Package.HOTEL_STARTUP_PROVIDES__TYPEMANAGER:
				if (resolve) return getTypemanager();
				return basicGetTypemanager();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group05Package.HOTEL_STARTUP_PROVIDES__ROOMMANAGER:
				setRoommanager((RoomManager)newValue);
				return;
			case Group05Package.HOTEL_STARTUP_PROVIDES__BOOKINGMANAGER:
				setBookingmanager((BookingManager)newValue);
				return;
			case Group05Package.HOTEL_STARTUP_PROVIDES__TYPEMANAGER:
				setTypemanager((TypeManager)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group05Package.HOTEL_STARTUP_PROVIDES__ROOMMANAGER:
				setRoommanager((RoomManager)null);
				return;
			case Group05Package.HOTEL_STARTUP_PROVIDES__BOOKINGMANAGER:
				setBookingmanager((BookingManager)null);
				return;
			case Group05Package.HOTEL_STARTUP_PROVIDES__TYPEMANAGER:
				setTypemanager((TypeManager)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group05Package.HOTEL_STARTUP_PROVIDES__ROOMMANAGER:
				return roommanager != null;
			case Group05Package.HOTEL_STARTUP_PROVIDES__BOOKINGMANAGER:
				return bookingmanager != null;
			case Group05Package.HOTEL_STARTUP_PROVIDES__TYPEMANAGER:
				return typemanager != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group05Package.HOTEL_STARTUP_PROVIDES___STARTUP__INT:
				startup((Integer)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelStartupProvidesImpl
