/**
 */
package se.chalmers.cse.mdsd1617.group05.impl;

import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import javax.xml.soap.SOAPException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import se.chalmers.cse.mdsd1617.group05.Booking;
import se.chalmers.cse.mdsd1617.group05.BookingManager;
import se.chalmers.cse.mdsd1617.group05.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group05.Group05Factory;
import se.chalmers.cse.mdsd1617.group05.Group05Package;
import se.chalmers.cse.mdsd1617.group05.Room;
import se.chalmers.cse.mdsd1617.group05.RoomManager;
import se.chalmers.cse.mdsd1617.group05.RoomReservation;
import se.chalmers.cse.mdsd1617.group05.Status;
import se.chalmers.cse.mdsd1617.group05.Type;
import se.chalmers.cse.mdsd1617.group05.util.Utils;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Booking Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.BookingManagerImpl#getBooking <em>Booking</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.BookingManagerImpl#getCurrentCheckoutBooking <em>Current Checkout Booking</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.BookingManagerImpl#getInitiatedRoomCheckouts <em>Initiated Room Checkouts</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingManagerImpl extends MinimalEObjectImpl.Container implements BookingManager {
	/**
	 * The cached value of the '{@link #getBooking() <em>Booking</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBooking()
	 * @generated
	 * @ordered
	 */
	protected EList<Booking> booking;
	
	/**
	 * The cached value of the '{@link #getCurrentCheckoutBooking() <em>Current Checkout Booking</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentCheckoutBooking()
	 * @generated
	 * @ordered
	 */
	protected Booking currentCheckoutBooking;

	/**
	 * The cached value of the '{@link #getInitiatedRoomCheckouts() <em>Initiated Room Checkouts</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitiatedRoomCheckouts()
	 * @generated
	 * @ordered
	 */
	protected EList<RoomReservation> initiatedRoomCheckouts;

	protected Group05Factory factory = Group05FactoryImpl.init();
	
	protected se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires banking;

	private static BookingManagerImpl instance = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	private BookingManagerImpl() {
		super();
		initiatedRoomCheckouts = new BasicEList<RoomReservation>();
	}

	/**
	 * Get bookingManager instance
	 * @return instance
	 */
	public static BookingManagerImpl getInstance(){
		if (instance == null){
			instance = new BookingManagerImpl();
		}
		return instance;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group05Package.Literals.BOOKING_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Booking> getBooking() {
		if (booking == null) {
			booking = new EObjectResolvingEList<Booking>(Booking.class, this, Group05Package.BOOKING_MANAGER__BOOKING);
		}
		return booking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Booking getCurrentCheckoutBooking() {
		if (currentCheckoutBooking != null && currentCheckoutBooking.eIsProxy()) {
			InternalEObject oldCurrentCheckoutBooking = (InternalEObject)currentCheckoutBooking;
			currentCheckoutBooking = (Booking)eResolveProxy(oldCurrentCheckoutBooking);
			if (currentCheckoutBooking != oldCurrentCheckoutBooking) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group05Package.BOOKING_MANAGER__CURRENT_CHECKOUT_BOOKING, oldCurrentCheckoutBooking, currentCheckoutBooking));
			}
		}
		return currentCheckoutBooking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Booking basicGetCurrentCheckoutBooking() {
		return currentCheckoutBooking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentCheckoutBooking(Booking newCurrentCheckoutBooking) {
		Booking oldCurrentCheckoutBooking = currentCheckoutBooking;
		currentCheckoutBooking = newCurrentCheckoutBooking;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group05Package.BOOKING_MANAGER__CURRENT_CHECKOUT_BOOKING, oldCurrentCheckoutBooking, currentCheckoutBooking));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoomReservation> getInitiatedRoomCheckouts() {
		if (initiatedRoomCheckouts == null) {
			initiatedRoomCheckouts = new EObjectResolvingEList<RoomReservation>(RoomReservation.class, this, Group05Package.BOOKING_MANAGER__INITIATED_ROOM_CHECKOUTS);
		}
		return initiatedRoomCheckouts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * @author Marco Trifance
	 * @purpose Implements Use Case 2.1.2.
	 * 			Returns an EList of FreeRoomTypesDTO objects containing information
	 * 			on the Type and number of available rooms within given dates and 
	 * 			with at least 'numBeds'. Returns an empty EList if startDate is not before endDate.   
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate) {
		
		//1. Validate dates. Return empty EList if invalid
		if(!Utils.validateStringDates(startDate, endDate)){
			return new BasicEList<FreeRoomTypesDTO>();
		}
		
		// 2. Get unblocked rooms info from RoomManager
		RoomManager roomManager = factory.createRoomManager();
		EList<FreeRoomTypesDTO> unblockedRoomsInfo = roomManager.getUnblockedRooms(numBeds);
		
		// 3. Reduce the count of available rooms by the number of RoomReservations with 
		//    same roomType and overlapping dates.
		return filterAvailableRooms(unblockedRoomsInfo, startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * @author 		Marco Trifance
	 * @purpose  	Validates provided input parameters. If validation is successful,
	 * 				creates an UNCONFIRMED, UNPAID booking with a unique integer ID,
	 * 				provided dates and customer information. The Booking object is appended 
	 * 				to the list of existing bookings. 
	 * 				Operation is aborted if input validation fails.
	 * @return		The unique integer ID if the operation is successful, -1 otherwise
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {
		if(!Utils.validateStringDates(startDate, endDate) || !Utils.validateName(firstName, lastName))
			return -1;
		
		// Create a booking 
		Booking b = factory.createBooking();
		
		// Set unique ID and provided dates.
		int bookingID = Utils.generateUniqueID();
		b.setId(bookingID);
		b.setFirstName(firstName);
		b.setLastName(lastName);
		b.setDateIn(Utils.getDateFromString(startDate));
		b.setDateOut(Utils.getDateFromString(endDate));
		getBooking().add(b);
		return bookingID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * @author 		Marco Trifance
	 * @purpose 	Adds a room of the given type to the Booking with given bookingID.
	 * 				Returns true if operation is successful. Returns false if any of the
	 * 				following holds:
	 * 				- bookingId is not existing;
	 * 				- booking is already confirmed;
	 * 				- typeDescription do not match an existing Type in TypeManager.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		
		// 1. Fetch Type from TypeManager. Return false if type is NOT existing
		Type type = factory.createTypeManager().fetchTypeByName(roomTypeDescription);
		if(type == null){
			return false;
		}
		
		// 2. Fetch Booking. Return false if Booking was NOT FOUND or if CONFIRMED already
		Booking b = fetchBooking(bookingID);
		if(b == null || b.isConfirmed()) {return false;}
		
		// 3. Check Availability
		EList<FreeRoomTypesDTO> freeRooms = getFreeRooms(type.getNumberOfBeds(), b.getStringDateIn(), b.getStringDateOut());		
		for(FreeRoomTypesDTO fr:freeRooms){
			if(fr.getRoomTypeDescription() == type.getName() && fr.getNumFreeRooms() > 0){
				// Add RoomReservation of given type in Booking and return true 
				b.addRoomReservation(type);
				return true;
			}
		}
		return false;		// Room was not available
	}
	
	/**
	  * <!-- begin-user-doc -->
	  * @author Alexander
	 * Checks all bookings for the right bookingID.
	 * Then checks if there is a atleast one free room of the correct type available during the given dates.
	 * If there is, create a new roomreservation of that type and add it to the booking.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addTypeToBooking(int bookingID, String typeName) {
		//Check all the bookings for the given ID
		for(int i = 0; i < getAllBookings().size(); i++){
			if(getAllBookings().get(i).getId() == bookingID){
				Booking tempBooking = getAllBookings().get(i);
				
				EList<FreeRoomTypesDTO> freeRooms = getFreeRooms(0, Utils.getStringDate(tempBooking.getDateIn()), Utils.getStringDate(tempBooking.getDateOut()));
				for(int j = 0; j < freeRooms.size(); j++){
					if(freeRooms.get(j).getRoomTypeDescription() == typeName && freeRooms.get(j).getNumFreeRooms() >= 1){
						RoomReservation tempRes = factory.createRoomReservation();
						tempRes.setType(factory.createTypeManager().fetchTypeByName(freeRooms.get(j).getRoomTypeDescription()));
						//If we didn't manage to find the right type
						if(tempRes.getType() == null)
							return false;
						
						tempBooking.getRoomreservation().add(tempRes);

						return true;
					}
				}
			}
		}
		
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * @author Johan
	 * 
	 * Returns true if booking exists and booking have at least one roomRes, false otherwise
	 * If it returns true it also sets isConfirmed to true in booking
	 * 
	 * 
	 * I implemented this to get my tests working, feel free to change
	 * 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		for (Booking b : booking){
			if (b.getId() == bookingID){
				if (b.getRoomreservation().size() > 0){
					b.setIsConfirmed(true);
					return true; //Success
				}
				return false; //Booking have no roomRes
			}
		}
		return false; //Booking not found
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * @author Johan
	 * 
	 * Returns -1 if bookingID doesn't exist, -2 if another booking currently is checking out
	 * 
	 * 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateCheckout(int bookingID) {
		
		if (currentCheckoutBooking != null){
			return -2; //Another booking is checking out
		}
		
		//Find the right booking
		for (Booking b : booking){
			if (b.getId() == bookingID){
				currentCheckoutBooking = b;
			}
		}
		
		//Return -1 if there don't exist a booking with the right bookingID
		if (currentCheckoutBooking == null){
			return -1;
		}
		
		return currentCheckoutBooking.getTotalPrice();
	}

	/**
	 * <!-- begin-user-doc -->
	 * @author Johan
	 * 
	 * 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		int loops = 0;
		boolean stopLoop = false;
		boolean payCompleted = false;
		
		if (currentCheckoutBooking == null){
			return false; //No checkout initiated
		}
		
		//Init bank instance
		while(loops < 5 && !stopLoop){ //Loops up to five times if communication with bank goes wrong
			try {
				banking  = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires.instance();
				stopLoop = true;
				
			} catch (SOAPException e) {
				System.err.println("Error occurred while communicating with the bank: 1:" + loops);
				e.printStackTrace();
				loops++;
			}
		}
				
		if (!stopLoop){
			return false; //No contact with the bank
		}
		//end init bank instance
		
		
		//Try to make payment
		loops = 0;
		stopLoop = false;
		
		while(loops < 5  && !stopLoop){ //Loops up to five times if communication with bank goes wrong
			try{
				if(banking.isCreditCardValid(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName)){
					payCompleted = banking.makePayment(ccNumber, ccv, expiryMonth, 
							expiryYear, firstName, lastName, currentCheckoutBooking.getTotalPrice());
					stopLoop = true;
					
				} else {
					return false; //Creditcard is not valid
				}
			} catch (SOAPException e){
				System.err.println("Error occurred while communicating with the bank: 2:" + loops);
				e.printStackTrace();
				loops++;
			}
		}
		
		if (!payCompleted){
			return false; //Communication with bank failed five times or payment didn't go through
		}
		//end try to make payment
		
		//Check out and flag that rooms are paid
		
		//Get all roomReservations in the booking
		EList<RoomReservation> roomResBelongingToBooking = currentCheckoutBooking.getRoomreservation();
				
		//Check out all room belonging to booking that hasn't been checked out yet and set isPaid = true
		for (RoomReservation rr : roomResBelongingToBooking){
			if (rr.getCheckOut() == null){
				rr.setCheckOut(new Timestamp(System.currentTimeMillis()));
				rr.setIsPaid(true);
				rr.getRoom().setStatus(Status.FREE);
				
			}
		}
		//end Check out and flag that rooms are paid
		
		currentCheckoutBooking = null;
		
		return true; //payment successful
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * @author Johan
	 * 
	 * Returns -1 if bookingID can't be found, -2 if roomNumber doesn't belong to bookingID, 
	 * -3 if room already is checked out, -4 if the given room doesn't exist. 
	 * If everything goes right it returns the price.
	 * 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateRoomCheckout(int roomNumber, int bookingID) {
		
		//Check that given room exists
		EList<Room> allRooms = factory.createRoomManager().getRooms();
		boolean roomExists = false;
		
		for (Room r : allRooms){
			if (r.getNumber() == roomNumber){
				roomExists = true;
				break;
			}
		}
		
		if(!roomExists){
			return -4; //Given room doesn't exist
		}
		//end Check that given room exists
		
		Booking currentBooking = null;
		
		//Find the right booking
		for (Booking b : booking){
			if (b.getId() == bookingID){
				currentBooking = b;
			}
		}
		
		//Return -1 if there don't exist a booking with the right bookingID
		if (currentBooking == null){
			return -1;
		}
		
		//Get all roomReservations in the booking
		EList<RoomReservation> roomResBelongingToBooking = currentBooking.getRoomreservation();
		
		RoomReservation roomResToCheckOut = null;
		
		//Find the roomRes with the right room number
		for (RoomReservation rr : roomResBelongingToBooking){
			if (roomNumber == rr.getRoom().getNumber()){
				roomResToCheckOut = rr;
				break;
			}
		}
		
		//Return -2 if there isn't a room with that room number connected to the booking
		if (roomResToCheckOut == null){
			return -2;
		}
		
		//Return -3 if the room already is checked out
		if (roomResToCheckOut.getCheckOut() != null){
			return -3;
		}
		
		initiatedRoomCheckouts.add(roomResToCheckOut);
		return roomResToCheckOut.getPrice();
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * @author Johan
	 * 
	 * Pays and checks out the room with the given room number
	 * 
	 * Returns false if communication with bank failed, creditcard is not valid or payment didn't go through
	 * Returns true if the payment is successful
	 * 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		
		RoomReservation roomResToPay = null;
		
		//Find the right roomRes to check out
		for(RoomReservation rr : initiatedRoomCheckouts){
			if(rr.getRoom().getNumber() == roomNumber){
				roomResToPay = rr;
				break;
			}
		}
		
		if(roomResToPay == null || roomResToPay.isPaid()){
			return false; //Checkout for the room not initiated or already paid
		}
		
		int loops = 0;
		boolean stopLoop = false;
		boolean payCompleted = false;
		
		//Init bank instance
		while(loops < 5 && !stopLoop){ //Loops up to five times if communication with bank goes wrong
			try {
				banking  = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires.instance();
				stopLoop = true;
				
			} catch (SOAPException e) {
				System.err.println("Error occurred while communicating with the bank: 1:" + loops);
				e.printStackTrace();
				loops++;
			}
		}
				
		if (!stopLoop){
			return false; //No contact with the bank
		}
		
		//end init bank instance
		
		
		//Try to make payment
		loops = 0;
		stopLoop = false;
		
		while(loops < 5  && !stopLoop){ //Loops up to five times if communication with bank goes wrong
			try{
				if(banking.isCreditCardValid(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName)){
					payCompleted = banking.makePayment(ccNumber, ccv, expiryMonth, 
							expiryYear, firstName, lastName, roomResToPay.getPrice());
					stopLoop = true;
					
				} else {
					return false; //Creditcard is not valid
				}
			} catch (SOAPException e){
				System.err.println("Error occurred while communicating with the bank: 2:" + loops);
				e.printStackTrace();
				loops++;
			}
		}
		
		if (!payCompleted){
			return false; //Communication with bank failed five times or payment didn't go through
		}
		//End try to make payment
		
		//Flag that the room is checked out and paid
		
		roomResToPay.setCheckOut(new Timestamp(System.currentTimeMillis()));
		roomResToPay.setIsPaid(true);
		roomResToPay.getRoom().setStatus(Status.FREE);
		initiatedRoomCheckouts.remove(roomResToPay);
		
		return true; //Payment successful
	}

	/**
	 * <!-- begin-user-doc -->
	 * @author Johan
	 * 
	 * Returns the room number of the checked in room if successful, 
	 * -1 if the bookingID doesn't exist,
	 * -2 if roomRes of given type wasn't found in the booking.
	 * 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(String roomTypeDescription, int bookingID) {
		
		Booking currentRoomCheckoutBooking = null;
		
		//Find the right booking
		for (Booking b : booking){
			if (b.getId() == bookingID){
				currentRoomCheckoutBooking = b;
			}
		}
		
		//Return -1 if there don't exist a booking with the right bookingID
		if (currentRoomCheckoutBooking == null){
			return -1;
		}
		
		//Get all roomReservations in the booking
		EList<RoomReservation> roomResToCheckIn = currentRoomCheckoutBooking.getRoomreservation();
		
		//Get all free rooms
		EList<Room> allFreeRooms = factory.createRoomManager().getFreeRooms();
		
		for (RoomReservation rr : roomResToCheckIn){
			if(rr.getType().getName() == roomTypeDescription && rr.getRoom() == null){
				for (Room r : allFreeRooms){
					if (r.getType().getName() == roomTypeDescription && r.getStatus() == Status.FREE){
						r.setStatus(Status.OCCUPIED);
						rr.setRoom(r);
						rr.setCheckIn(new Timestamp(System.currentTimeMillis()));
						return r.getNumber();
						
					}
				}
			}	
		}
		
		return -2; //No roomRes found of given type or no free room of given type found
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * @author Johan
	 * 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkInBooking(int bookingID) {
		
		Booking currentBooking = null;
		
		//Find the right booking
		for (Booking b : booking){
			if (b.getId() == bookingID){
				currentBooking = b;
			}
		}
		
		//Return null if there don't exist a booking with the right bookingID
		if (currentBooking == null){
			return false;
		}
		
		
		//Get all rooms
		EList<Room> allRooms = factory.createRoomManager().getFreeRooms();
		
		//Get all roomReservations in the booking
		EList<RoomReservation> roomResToCheckIn = currentBooking.getRoomreservation();
		
		EList<Room> availableRooms = new BasicEList<Room>();
		Room chosenRoom = null;
		
		//Loops through all roomReservations
		for (RoomReservation rr : roomResToCheckIn){
			if(rr.getCheckIn() == null){
				availableRooms.clear();
				//Find rooms with the right type
				for (Room r : allRooms){
					if (r.getType().equals(rr.getType()) && r.getStatus() == Status.FREE){
						availableRooms.add(r);
					}
				}
				
				//Show the available rooms to the user
				
				//The user shows which room he/she wants to check in
				
				//Right now we just pick the first room in the list instead of getting any input if there are any available rooms (which it should be)
				if (availableRooms.size() > 0){
					chosenRoom = availableRooms.remove(0);
				}else{
					return false; //Didn't find a free room of the right type to check in (shouldn't be able to get here if booking is done correctly)
				}
				
				chosenRoom.setStatus(Status.OCCUPIED);
				rr.setRoom(chosenRoom);
				rr.setCheckIn(new Timestamp(System.currentTimeMillis()));
			}
		}//End roomReservation loop
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * @author Alexander
	 * Checks all bookings for the right bookingID.
	 * If it is found all the reservations of that booking is checked for the right type. 
	 * If a matching reservation is found, remove it from the booking and return true.
	 * If no booking/reservation is found return false.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeTypeFromBooking(int bookingID, String typeName) {
		//Check all the bookings for the given ID
		for(int i = 0; i < getAllBookings().size(); i++){
			if(getAllBookings().get(i).getId() == bookingID){
				
				//Check all reservations for the given type
				for(int j = 0; j < getAllBookings().get(i).getRoomreservation().size(); j++){
					RoomReservation tempRes = getAllBookings().get(i).getRoomreservation().get(j);		
					if(tempRes.getType().getName() == typeName){
						getAllBookings().get(i).getRoomreservation().remove(j);
						
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * @author Alexander
	 * Checks all bookings for the right bookingID.
	 * If it is found, set the expected in and out dates to the new values. Otherwise return false.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean setBookingExpectedDates(int bookingID, String inDate, String outDate) {
		for(int i = 0; i < getAllBookings().size(); i++){
			if(getAllBookings().get(i).getId() == bookingID){
				getAllBookings().get(i).setDateIn(Utils.getDateFromString(inDate));
				getAllBookings().get(i).setDateOut(Utils.getDateFromString(outDate));
				return true;
			}
		}
		
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * @author Alexander
	 * Checks all bookings for the right bookingID.
	 * If it is found, remove the booking element and return true. Otherwise return false.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean cancelBooking(int bookingID) {
		for(int i = 0; i < getAllBookings().size(); i++){
			if(getAllBookings().get(i).getId() == bookingID){
				getAllBookings().remove(i);
				return true;
			}
		}
	
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * @author Alexander
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> getAllBookings() {
		return getBooking();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Joakim
	 * @generated NOT
	 */
	public EList<RoomReservation> getOccupiedRooms(Date date) {
		EList<RoomReservation> reservations = new BasicEList<RoomReservation>();
		for (Booking singleBooking : booking){
			for (RoomReservation reservation : singleBooking.getRoomreservation()){
				//Reservation has a check-in date and either checkout has passed or it has no check-out date, meaning it is still checked in. 
				if (reservation.getCheckIn() != null && (reservation.getCheckIn().before(date) || reservation.getCheckIn().equals(date))  && (reservation.getCheckOut() == null || reservation.getCheckOut().after(date))){
					reservations.add(reservation);
				}
			}
		}
		return reservations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Joakim
	 * @generated NOT
	 */
	public EList<RoomReservation> getCheckIns(String startDate, String endDate) {
		//Uses private helper-method with argument 0 (checkIns) to return checkIns in the timespan between startDate and endDate
		return getCheckInsCheckOutsForDay(0, startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @author Joakim
	 * @generated NOT
	 */
	public EList<RoomReservation> getCheckOuts(String startDate, String endDate) {
		//Uses private helper-method with argument 1 (checkOuts) to return checkOuts in the timespan between startDate and endDate
		return getCheckInsCheckOutsForDay(1, startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * @author 		Marco Trifance
	 * @purpose		Applies the extra-cost with the provided 'amount' and 'description'
	 * 				to the RoomReservation that reserves the Room with the provided 'roomNumber'.
	 * 				Returns false if the Booking with the provided 'bookingId' does not exist or 
	 * 				does not include a RoomReservation for the given RoomNumber.
	 * 				Returns true if operation is successful.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addCostToRoom(int roomNumber, int bookingID, String costDescription, double amount) {
		// 1. Fetch booking - Return false if not found
		Booking b = fetchBooking(bookingID);
		
		// Return false if booking was not found
		if(b == null) {return false;}
				
		// 2. Find RoomReservation with matching room#
		if(b.addCostToRoom(amount, roomNumber, costDescription)){
			return true;
		}else{
			return false;
		}	
	}

	/**
	 * <!-- begin-user-doc -->
	 * Private helper-method to return a list of checkIns or checkOuts in a given timespan depending on the action parameter. 
	 * <!-- end-user-doc -->
	 * @author Joakim
	 * @generated NOT
	 */
	public EList<RoomReservation> getCheckInsCheckOutsForDay(int action, String startDate, String endDate) {
		EList<RoomReservation> reservations = new BasicEList<RoomReservation>();
		Date start = Utils.getDateFromString(startDate);
		Date end = Utils.getDateFromString(endDate);
		for (Booking singleBooking : booking){
			for (RoomReservation reservation : singleBooking.getRoomreservation()){
				if (action == 0 && reservation.getCheckIn().after(start) && reservation.getCheckIn().before(end)){
					reservations.add(reservation);
				} else if(action == 1 && reservation.getCheckOut().after(start) && reservation.getCheckOut().before(end)){
					reservations.add(reservation);
				}
			}
		}
		return reservations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean clearBookings() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		//throw new UnsupportedOperationException();
		
		getBooking().clear();
		if (getBooking().size() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * @author Marco Trifance
	 * @purpose Returns a list of FreeRoomTypesDTO objects after decreasing the 
	 * 			count of available rooms in each item by the number of RoomReservations 
	 * 			that have same room type and overlapping dates. 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<FreeRoomTypesDTO> filterAvailableRooms(EList<FreeRoomTypesDTO> toFilter, String startDate, String endDate) {
	
		// 1. Get an EList of bookings within given dates
		EList <Booking> overlappingBookings = getOverlappingBookings(startDate, endDate);
				
		// 2. Get an EList of all 'room reservations' included in the 'overlapping bookings' 
		EList <RoomReservation> overlappingReservations = new BasicEList<RoomReservation>();
				
		for (int i = 0; i < overlappingBookings.size(); i++){
			overlappingReservations.addAll(overlappingBookings.get(i).getRoomreservation());
		}
				
		// 3. For each element in 'toFilter'
		for(int j = 0; j < toFilter.size(); j++){
			FreeRoomTypesDTO roomTypeDTO = toFilter.get(j);
			// Loop through all overlapping reservations...
			for(int h = 0; h < overlappingReservations.size(); h++){		
				// If type name in reservation matches type description in DTO 
				if (overlappingReservations.get(h).getType().getName() == roomTypeDTO.getRoomTypeDescription()){
					int oldCount = roomTypeDTO.getNumFreeRooms();		
					// Handle OVERBOOKING
					if(oldCount == 0){
						System.out.println("Overbooking: ");
					}
					// Decrease free-rooms counter in roomTypeDTO
					roomTypeDTO.setNumFreeRooms(oldCount - 1);
				} 
			}
		}		
		return toFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * @author Marco Trifance
	 * @purpose Returns an EList of bookings that overlap with period 
	 * 			between 'startDate' and 'endDate'.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> getOverlappingBookings(String startDate, String endDate) {
		
		// Initialize return value
		EList<Booking> overlappingBookings = new BasicEList<Booking>();
				
		// Get reference to booking EList
		EList<Booking> bookingList = getBooking();
				
		// For each booking in the Booking Manager
		for(int i = 0; i < bookingList.size(); i++){
			Booking tempBooking = bookingList.get(i);
			boolean cond1 = Utils.isDateIncluded(tempBooking.getDateIn(), startDate, endDate) 
							|| Utils.isDateIncluded(tempBooking.getDateOut(), startDate, endDate); 
					
			boolean cond2 = Utils.isDateIncluded(Utils.getDateFromString(startDate), tempBooking.getDateIn(), tempBooking.getDateOut())
							|| Utils.isDateIncluded(Utils.getDateFromString(endDate), tempBooking.getDateIn(), tempBooking.getDateOut());
				
			boolean overlap = cond1 || cond2;
				
			// If booking is overlapping, add it to EList 
			if(overlap){
				overlappingBookings.add(tempBooking);
			}
		}
		return overlappingBookings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * @author 		Marco Trifance
	 * @purpose 	Returns the Booking object stored with the provided ID.
	 * 				Returns null if the provided ID does not exist.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Booking fetchBooking(int id) {
		Booking b = null;
		for(Booking book:getBooking()){
			if(book.getId() == id){
				b = book;
				break;
			}
		}
		return b;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group05Package.BOOKING_MANAGER__BOOKING:
				return getBooking();
			case Group05Package.BOOKING_MANAGER__CURRENT_CHECKOUT_BOOKING:
				if (resolve) return getCurrentCheckoutBooking();
				return basicGetCurrentCheckoutBooking();
			case Group05Package.BOOKING_MANAGER__INITIATED_ROOM_CHECKOUTS:
				return getInitiatedRoomCheckouts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group05Package.BOOKING_MANAGER__BOOKING:
				getBooking().clear();
				getBooking().addAll((Collection<? extends Booking>)newValue);
				return;
			case Group05Package.BOOKING_MANAGER__CURRENT_CHECKOUT_BOOKING:
				setCurrentCheckoutBooking((Booking)newValue);
				return;
			case Group05Package.BOOKING_MANAGER__INITIATED_ROOM_CHECKOUTS:
				getInitiatedRoomCheckouts().clear();
				getInitiatedRoomCheckouts().addAll((Collection<? extends RoomReservation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group05Package.BOOKING_MANAGER__BOOKING:
				getBooking().clear();
				return;
			case Group05Package.BOOKING_MANAGER__CURRENT_CHECKOUT_BOOKING:
				setCurrentCheckoutBooking((Booking)null);
				return;
			case Group05Package.BOOKING_MANAGER__INITIATED_ROOM_CHECKOUTS:
				getInitiatedRoomCheckouts().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group05Package.BOOKING_MANAGER__BOOKING:
				return booking != null && !booking.isEmpty();
			case Group05Package.BOOKING_MANAGER__CURRENT_CHECKOUT_BOOKING:
				return currentCheckoutBooking != null;
			case Group05Package.BOOKING_MANAGER__INITIATED_ROOM_CHECKOUTS:
				return initiatedRoomCheckouts != null && !initiatedRoomCheckouts.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group05Package.BOOKING_MANAGER___GET_FREE_ROOMS__INT_STRING_STRING:
				return getFreeRooms((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case Group05Package.BOOKING_MANAGER___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case Group05Package.BOOKING_MANAGER___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case Group05Package.BOOKING_MANAGER___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case Group05Package.BOOKING_MANAGER___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
			case Group05Package.BOOKING_MANAGER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case Group05Package.BOOKING_MANAGER___INITIATE_ROOM_CHECKOUT__INT_INT:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case Group05Package.BOOKING_MANAGER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case Group05Package.BOOKING_MANAGER___CHECK_IN_ROOM__STRING_INT:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
			case Group05Package.BOOKING_MANAGER___CHECK_IN_BOOKING__INT:
				return checkInBooking((Integer)arguments.get(0));
			case Group05Package.BOOKING_MANAGER___REMOVE_TYPE_FROM_BOOKING__INT_STRING:
				return removeTypeFromBooking((Integer)arguments.get(0), (String)arguments.get(1));
			case Group05Package.BOOKING_MANAGER___SET_BOOKING_EXPECTED_DATES__INT_STRING_STRING:
				return setBookingExpectedDates((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case Group05Package.BOOKING_MANAGER___CANCEL_BOOKING__INT:
				return cancelBooking((Integer)arguments.get(0));
			case Group05Package.BOOKING_MANAGER___GET_ALL_BOOKINGS:
				return getAllBookings();
			case Group05Package.BOOKING_MANAGER___GET_OCCUPIED_ROOMS__DATE:
				return getOccupiedRooms((Date)arguments.get(0));
			case Group05Package.BOOKING_MANAGER___GET_CHECK_INS__STRING_STRING:
				return getCheckIns((String)arguments.get(0), (String)arguments.get(1));
			case Group05Package.BOOKING_MANAGER___GET_CHECK_OUTS__STRING_STRING:
				return getCheckOuts((String)arguments.get(0), (String)arguments.get(1));
			case Group05Package.BOOKING_MANAGER___ADD_COST_TO_ROOM__INT_INT_STRING_DOUBLE:
				return addCostToRoom((Integer)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Double)arguments.get(3));
			case Group05Package.BOOKING_MANAGER___ADD_TYPE_TO_BOOKING__INT_STRING:
				return addTypeToBooking((Integer)arguments.get(0), (String)arguments.get(1));
			case Group05Package.BOOKING_MANAGER___GET_CHECK_INS_CHECK_OUTS_FOR_DAY__INT_STRING_STRING:
				return getCheckInsCheckOutsForDay((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case Group05Package.BOOKING_MANAGER___CLEAR_BOOKINGS:
				return clearBookings();
			case Group05Package.BOOKING_MANAGER___FILTER_AVAILABLE_ROOMS__ELIST_STRING_STRING:
				return filterAvailableRooms((EList<FreeRoomTypesDTO>)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case Group05Package.BOOKING_MANAGER___GET_OVERLAPPING_BOOKINGS__STRING_STRING:
				return getOverlappingBookings((String)arguments.get(0), (String)arguments.get(1));
			case Group05Package.BOOKING_MANAGER___FETCH_BOOKING__INT:
				return fetchBooking((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //BookingManagerImpl
