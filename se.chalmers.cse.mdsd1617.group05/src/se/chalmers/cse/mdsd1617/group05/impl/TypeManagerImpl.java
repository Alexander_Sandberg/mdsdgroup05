/**
 */
package se.chalmers.cse.mdsd1617.group05.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import se.chalmers.cse.mdsd1617.group05.Group05Factory;
import se.chalmers.cse.mdsd1617.group05.Group05Package;
import se.chalmers.cse.mdsd1617.group05.ITypeManager;
import se.chalmers.cse.mdsd1617.group05.Type;
import se.chalmers.cse.mdsd1617.group05.TypeManager;
import se.chalmers.cse.mdsd1617.group05.Room;
/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.TypeManagerImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TypeManagerImpl extends MinimalEObjectImpl.Container implements TypeManager {
	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected EList<Type> type;
	
	private static Group05Factory factory = Group05FactoryImpl.init();
    
	private static TypeManagerImpl instance = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	
	
	private TypeManagerImpl() {
		super();
	}

	/**
	 * Get typeManager instance
	 * @return instance
	 */
	public static TypeManagerImpl getInstance(){
		if (instance == null){
			instance = new TypeManagerImpl();
		}
		return instance;
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group05Package.Literals.TYPE_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Type> getType() {
		if (type == null) {
			type = new EObjectResolvingEList<Type>(Type.class, this, Group05Package.TYPE_MANAGER__TYPE);
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomType(String typeName, double price, int numOfBeds) {
		for(Type t:getType())
		{
			if(t.getName()==typeName)
			{
				return false;
			}
		}
		
		Type newtype = factory.createType();
		newtype.setName(typeName);
		newtype.setPrice(price);
		newtype.setNumberOfBeds(numOfBeds);
		
		type.add(newtype);
		return true;
		
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateRoomType(String oldName, String newName, double newPrice, int newNumOfBeds) {
		if(oldName!=newName)
		{
			for(Type t:getType())
				if(t.getName()==newName)
					return false;
		}
			
		
		for(Type t:getType())
		{
			if(t.getName()==oldName)
			{
				t.setName(newName);
				t.setNumberOfBeds(newNumOfBeds);
				t.setPrice(newPrice);
				return true;
			};
		}
		
		return false;
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoomType(String typeName) {
		for(Room r: RoomManagerImpl.getInstance().getRoom())
		{
			if(r.getType().getName()==typeName)
			{
				return false;
			}
		}
		
		int n=0;
		for(Type t:type)
		{
			if(t.getName()==typeName)
			{
				type.remove(n);
				return true;
			}
			n++;
		}
		
		return false;
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Type> getTypes() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		return getType();
		//throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * @author Alexander
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Type fetchTypeByName(String typeName) {
		for(Type t:getTypes()){
			if(t.getName() == typeName){
				return t;
			}
		}
		
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean clearTypes() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		//throw new UnsupportedOperationException();
		
		getType().clear();
		if (getType().size() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group05Package.TYPE_MANAGER__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group05Package.TYPE_MANAGER__TYPE:
				getType().clear();
				getType().addAll((Collection<? extends Type>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group05Package.TYPE_MANAGER__TYPE:
				getType().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group05Package.TYPE_MANAGER__TYPE:
				return type != null && !type.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == ITypeManager.class) {
			switch (baseOperationID) {
				case Group05Package.ITYPE_MANAGER___GET_TYPES: return Group05Package.TYPE_MANAGER___GET_TYPES;
				case Group05Package.ITYPE_MANAGER___FETCH_TYPE_BY_NAME__STRING: return Group05Package.TYPE_MANAGER___FETCH_TYPE_BY_NAME__STRING;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group05Package.TYPE_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT:
				return addRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2));
			case Group05Package.TYPE_MANAGER___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT:
				return updateRoomType((String)arguments.get(0), (String)arguments.get(1), (Double)arguments.get(2), (Integer)arguments.get(3));
			case Group05Package.TYPE_MANAGER___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
			case Group05Package.TYPE_MANAGER___GET_TYPES:
				return getTypes();
			case Group05Package.TYPE_MANAGER___FETCH_TYPE_BY_NAME__STRING:
				return fetchTypeByName((String)arguments.get(0));
			case Group05Package.TYPE_MANAGER___CLEAR_TYPES:
				return clearTypes();
		}
		return super.eInvoke(operationID, arguments);
	}

} //TypeManagerImpl
