/**
 */
package se.chalmers.cse.mdsd1617.group05.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Date;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import se.chalmers.cse.mdsd1617.group05.Booking;
import se.chalmers.cse.mdsd1617.group05.Group05Factory;
import se.chalmers.cse.mdsd1617.group05.Group05Package;
import se.chalmers.cse.mdsd1617.group05.RoomReservation;
import se.chalmers.cse.mdsd1617.group05.Type;
import se.chalmers.cse.mdsd1617.group05.util.Utils;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Booking</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.BookingImpl#getRoomreservation <em>Roomreservation</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.BookingImpl#getId <em>Id</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.BookingImpl#getDateIn <em>Date In</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.BookingImpl#getDateOut <em>Date Out</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.BookingImpl#getFirstName <em>First Name</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.BookingImpl#getLastName <em>Last Name</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.impl.BookingImpl#isConfirmed <em>Is Confirmed</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingImpl extends MinimalEObjectImpl.Container implements Booking {
	/**
	 * The cached value of the '{@link #getRoomreservation() <em>Roomreservation</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomreservation()
	 * @generated
	 * @ordered
	 */
	protected EList<RoomReservation> roomreservation;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected int id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDateIn() <em>Date In</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateIn()
	 * @generated
	 * @ordered
	 */
	protected static final Date DATE_IN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDateIn() <em>Date In</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateIn()
	 * @generated
	 * @ordered
	 */
	protected Date dateIn = DATE_IN_EDEFAULT;

	/**
	 * The default value of the '{@link #getDateOut() <em>Date Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateOut()
	 * @generated
	 * @ordered
	 */
	protected static final Date DATE_OUT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDateOut() <em>Date Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateOut()
	 * @generated
	 * @ordered
	 */
	protected Date dateOut = DATE_OUT_EDEFAULT;

	/**
	 * The default value of the '{@link #getFirstName() <em>First Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstName()
	 * @generated
	 * @ordered
	 */
	protected static final String FIRST_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFirstName() <em>First Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstName()
	 * @generated
	 * @ordered
	 */
	protected String firstName = FIRST_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getLastName() <em>Last Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastName()
	 * @generated
	 * @ordered
	 */
	protected static final String LAST_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLastName() <em>Last Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastName()
	 * @generated
	 * @ordered
	 */
	protected String lastName = LAST_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #isConfirmed() <em>Is Confirmed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConfirmed()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_CONFIRMED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isConfirmed() <em>Is Confirmed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConfirmed()
	 * @generated
	 * @ordered
	 */
	protected boolean isConfirmed = IS_CONFIRMED_EDEFAULT;

	protected Group05Factory factory = Group05FactoryImpl.init();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BookingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group05Package.Literals.BOOKING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoomReservation> getRoomreservation() {
		if (roomreservation == null) {
			roomreservation = new EObjectResolvingEList<RoomReservation>(RoomReservation.class, this, Group05Package.BOOKING__ROOMRESERVATION);
		}
		return roomreservation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(int newId) {
		int oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group05Package.BOOKING__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getDateIn() {
		return dateIn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDateIn(Date newDateIn) {
		Date oldDateIn = dateIn;
		dateIn = newDateIn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group05Package.BOOKING__DATE_IN, oldDateIn, dateIn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getDateOut() {
		return dateOut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDateOut(Date newDateOut) {
		Date oldDateOut = dateOut;
		dateOut = newDateOut;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group05Package.BOOKING__DATE_OUT, oldDateOut, dateOut));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstName(String newFirstName) {
		String oldFirstName = firstName;
		firstName = newFirstName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group05Package.BOOKING__FIRST_NAME, oldFirstName, firstName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastName(String newLastName) {
		String oldLastName = lastName;
		lastName = newLastName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group05Package.BOOKING__LAST_NAME, oldLastName, lastName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isConfirmed() {
		return isConfirmed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsConfirmed(boolean newIsConfirmed) {
		boolean oldIsConfirmed = isConfirmed;
		isConfirmed = newIsConfirmed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group05Package.BOOKING__IS_CONFIRMED, oldIsConfirmed, isConfirmed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * @author Johan
	 * 
	 * returns the total price of all rooms belonging to the booking that not is already paid
	 * 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double getTotalPrice() {
		double totalPrice = 0;
		
		for (RoomReservation rr : roomreservation){
			if (!rr.isPaid()){
				totalPrice += rr.getPrice();
			}
		}
		
		return totalPrice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * @author Marco Trifance
	 * @purpose Returns the string representation of 'inDate' in
	 * 			format "YYYYMMDD"
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getStringDateIn() {
		return Utils.getStringDate(dateIn);
	}

	/**
	 * <!-- begin-user-doc -->
	 * * @author Marco Trifance
	 * @purpose Returns the string representation of 'outDate' in
	 * 			format "YYYYMMDD"
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getStringDateOut() {
		return Utils.getStringDate(dateOut);
	}

	/**
	 * <!-- begin-user-doc -->
	 * @author		Marco Trifance
	 * @purpose		Adds the extra-cost information to the RoomReservation with provided 'roomNum'.
	 * 				Returns false if the Booking instance do not contain a RoomReservation for the 
	 * 				given room number.
	 * 				Returns true if operation is successful.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addCostToRoom(double amount, int roomNum, String description) {
		EList<RoomReservation> rrs = getRoomreservation();
		for(RoomReservation rr:rrs){
			if(rr.getRoom() != null && rr.getRoom().getNumber() == roomNum){
				rr.addExtraCost(amount, description);
				return true;
			}
		}
		// Room was not found
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * @author		Marco Trifance
	 * @purpose		Creates a RoomReservation of the given Type and stores it in this booking
	 * 				instance. 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean addRoomReservation(Type roomType) {
		RoomReservation rr = factory.createRoomReservation();
		rr.setBookingId(this.getId());
		rr.setType(roomType);
		getRoomreservation().add(rr);
		return true;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group05Package.BOOKING__ROOMRESERVATION:
				return getRoomreservation();
			case Group05Package.BOOKING__ID:
				return getId();
			case Group05Package.BOOKING__DATE_IN:
				return getDateIn();
			case Group05Package.BOOKING__DATE_OUT:
				return getDateOut();
			case Group05Package.BOOKING__FIRST_NAME:
				return getFirstName();
			case Group05Package.BOOKING__LAST_NAME:
				return getLastName();
			case Group05Package.BOOKING__IS_CONFIRMED:
				return isConfirmed();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group05Package.BOOKING__ROOMRESERVATION:
				getRoomreservation().clear();
				getRoomreservation().addAll((Collection<? extends RoomReservation>)newValue);
				return;
			case Group05Package.BOOKING__ID:
				setId((Integer)newValue);
				return;
			case Group05Package.BOOKING__DATE_IN:
				setDateIn((Date)newValue);
				return;
			case Group05Package.BOOKING__DATE_OUT:
				setDateOut((Date)newValue);
				return;
			case Group05Package.BOOKING__FIRST_NAME:
				setFirstName((String)newValue);
				return;
			case Group05Package.BOOKING__LAST_NAME:
				setLastName((String)newValue);
				return;
			case Group05Package.BOOKING__IS_CONFIRMED:
				setIsConfirmed((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group05Package.BOOKING__ROOMRESERVATION:
				getRoomreservation().clear();
				return;
			case Group05Package.BOOKING__ID:
				setId(ID_EDEFAULT);
				return;
			case Group05Package.BOOKING__DATE_IN:
				setDateIn(DATE_IN_EDEFAULT);
				return;
			case Group05Package.BOOKING__DATE_OUT:
				setDateOut(DATE_OUT_EDEFAULT);
				return;
			case Group05Package.BOOKING__FIRST_NAME:
				setFirstName(FIRST_NAME_EDEFAULT);
				return;
			case Group05Package.BOOKING__LAST_NAME:
				setLastName(LAST_NAME_EDEFAULT);
				return;
			case Group05Package.BOOKING__IS_CONFIRMED:
				setIsConfirmed(IS_CONFIRMED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group05Package.BOOKING__ROOMRESERVATION:
				return roomreservation != null && !roomreservation.isEmpty();
			case Group05Package.BOOKING__ID:
				return id != ID_EDEFAULT;
			case Group05Package.BOOKING__DATE_IN:
				return DATE_IN_EDEFAULT == null ? dateIn != null : !DATE_IN_EDEFAULT.equals(dateIn);
			case Group05Package.BOOKING__DATE_OUT:
				return DATE_OUT_EDEFAULT == null ? dateOut != null : !DATE_OUT_EDEFAULT.equals(dateOut);
			case Group05Package.BOOKING__FIRST_NAME:
				return FIRST_NAME_EDEFAULT == null ? firstName != null : !FIRST_NAME_EDEFAULT.equals(firstName);
			case Group05Package.BOOKING__LAST_NAME:
				return LAST_NAME_EDEFAULT == null ? lastName != null : !LAST_NAME_EDEFAULT.equals(lastName);
			case Group05Package.BOOKING__IS_CONFIRMED:
				return isConfirmed != IS_CONFIRMED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group05Package.BOOKING___GET_TOTAL_PRICE:
				return getTotalPrice();
			case Group05Package.BOOKING___GET_STRING_DATE_IN:
				return getStringDateIn();
			case Group05Package.BOOKING___GET_STRING_DATE_OUT:
				return getStringDateOut();
			case Group05Package.BOOKING___ADD_COST_TO_ROOM__DOUBLE_INT_STRING:
				return addCostToRoom((Double)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2));
			case Group05Package.BOOKING___ADD_ROOM_RESERVATION__TYPE:
				return addRoomReservation((Type)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", dateIn: ");
		result.append(dateIn);
		result.append(", dateOut: ");
		result.append(dateOut);
		result.append(", firstName: ");
		result.append(firstName);
		result.append(", lastName: ");
		result.append(lastName);
		result.append(", isConfirmed: ");
		result.append(isConfirmed);
		result.append(')');
		return result.toString();
	}

} //BookingImpl
