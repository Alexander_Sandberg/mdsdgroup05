/**
 */
package se.chalmers.cse.mdsd1617.group05;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Booking Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.BookingManager#getBooking <em>Booking</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.BookingManager#getCurrentCheckoutBooking <em>Current Checkout Booking</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.BookingManager#getInitiatedRoomCheckouts <em>Initiated Room Checkouts</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getBookingManager()
 * @model
 * @generated
 */
public interface BookingManager extends IHotelReceptionist {
	/**
	 * Returns the value of the '<em><b>Booking</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group05.Booking}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booking</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booking</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getBookingManager_Booking()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	EList<Booking> getBooking();

	/**
	 * Returns the value of the '<em><b>Current Checkout Booking</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current Checkout Booking</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Checkout Booking</em>' reference.
	 * @see #setCurrentCheckoutBooking(Booking)
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getBookingManager_CurrentCheckoutBooking()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Booking getCurrentCheckoutBooking();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group05.BookingManager#getCurrentCheckoutBooking <em>Current Checkout Booking</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Checkout Booking</em>' reference.
	 * @see #getCurrentCheckoutBooking()
	 * @generated
	 */
	void setCurrentCheckoutBooking(Booking value);

	/**
	 * Returns the value of the '<em><b>Initiated Room Checkouts</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group05.RoomReservation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initiated Room Checkouts</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initiated Room Checkouts</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getBookingManager_InitiatedRoomCheckouts()
	 * @model ordered="false"
	 * @generated
	 */
	EList<RoomReservation> getInitiatedRoomCheckouts();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" actionRequired="true" actionOrdered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	EList<RoomReservation> getCheckInsCheckOutsForDay(int action, String startDate, String endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false"
	 * @generated
	 */
	boolean clearBookings();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" toFilterMany="true" toFilterOrdered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	EList<FreeRoomTypesDTO> filterAvailableRooms(EList<FreeRoomTypesDTO> toFilter, String startDate, String endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	EList<Booking> getOverlappingBookings(String startDate, String endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" idRequired="true" idOrdered="false"
	 * @generated
	 */
	Booking fetchBooking(int id);

} // BookingManager
