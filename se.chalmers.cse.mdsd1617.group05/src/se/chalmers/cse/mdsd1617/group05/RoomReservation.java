/**
 */
package se.chalmers.cse.mdsd1617.group05;

import java.util.Date;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room Reservation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getType <em>Type</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getRoom <em>Room</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getCheckIn <em>Check In</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getCheckOut <em>Check Out</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#isPaid <em>Is Paid</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getBookingId <em>Booking Id</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getExtracost <em>Extracost</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getRoomReservation()
 * @model
 * @generated
 */
public interface RoomReservation extends EObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Type)
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getRoomReservation_Type()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Type getType();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Type value);

	/**
	 * Returns the value of the '<em><b>Room</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room</em>' reference.
	 * @see #setRoom(Room)
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getRoomReservation_Room()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Room getRoom();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getRoom <em>Room</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room</em>' reference.
	 * @see #getRoom()
	 * @generated
	 */
	void setRoom(Room value);

	/**
	 * Returns the value of the '<em><b>Check In</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Check In</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Check In</em>' attribute.
	 * @see #setCheckIn(Date)
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getRoomReservation_CheckIn()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Date getCheckIn();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getCheckIn <em>Check In</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Check In</em>' attribute.
	 * @see #getCheckIn()
	 * @generated
	 */
	void setCheckIn(Date value);

	/**
	 * Returns the value of the '<em><b>Check Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Check Out</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Check Out</em>' attribute.
	 * @see #setCheckOut(Date)
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getRoomReservation_CheckOut()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Date getCheckOut();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getCheckOut <em>Check Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Check Out</em>' attribute.
	 * @see #getCheckOut()
	 * @generated
	 */
	void setCheckOut(Date value);

	/**
	 * Returns the value of the '<em><b>Is Paid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Paid</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Paid</em>' attribute.
	 * @see #setIsPaid(boolean)
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getRoomReservation_IsPaid()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	boolean isPaid();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#isPaid <em>Is Paid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Paid</em>' attribute.
	 * @see #isPaid()
	 * @generated
	 */
	void setIsPaid(boolean value);

	/**
	 * Returns the value of the '<em><b>Booking Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booking Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booking Id</em>' attribute.
	 * @see #setBookingId(int)
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getRoomReservation_BookingId()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getBookingId();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getBookingId <em>Booking Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Booking Id</em>' attribute.
	 * @see #getBookingId()
	 * @generated
	 */
	void setBookingId(int value);

	/**
	 * Returns the value of the '<em><b>Extracost</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extracost</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extracost</em>' reference.
	 * @see #setExtracost(ExtraCost)
	 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getRoomReservation_Extracost()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ExtraCost getExtracost();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group05.RoomReservation#getExtracost <em>Extracost</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extracost</em>' reference.
	 * @see #getExtracost()
	 * @generated
	 */
	void setExtracost(ExtraCost value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true" ordered="false"
	 * @generated
	 */
	double getPrice();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" amountRequired="true" amountOrdered="false" descriptionRequired="true" descriptionOrdered="false"
	 * @generated
	 */
	boolean addExtraCost(double amount, String description);

} // RoomReservation
