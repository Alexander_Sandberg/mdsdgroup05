package se.chalmers.cse.mdsd1617.group05.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

public class Utils {
	
	// Used to generate unique integer IDs (UC 2.1.1) 
	private static final AtomicInteger idGenerator = new AtomicInteger();
	
	/* @doc 	Returns the string representation of the 'date' parameter
	 * 			in format "YYYYMMDD".
	 * @author	Marco Trifance
	 * */
	public static String getStringDate(Date date){
		String stringDate = "";
		DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		if(date != null)
			stringDate = formatter.format(date);	
		return stringDate;
	}
	
	/* @doc		Returns true if the provided String parameter represents a valid
	 * 			date, false otherwise.
	 * @author  Marco Trifance	
	 * */
	public static boolean isDate(String strDate){
		if(getDateFromString(strDate) == null){
			return false;
		}
		return true;
	}
	
	/*@doc 	   Returns a unique integer id (UC 2.1.1).   
	 * @author Marco Trifance
	 * */
	public static int generateUniqueID(){
		return idGenerator.getAndIncrement();
	}
	
	/* @doc		Returns false if at least one between 'firstName' and 'lastName' is
	 * 			an empty string, true otherwise.
	 * @author	Marco Trifance
	 * */
	public static boolean validateName(String firstName, String lastName){
		if(firstName == null || lastName == null) {return false;}
		
		return !(firstName.isEmpty() || lastName.isEmpty());
	}
	
	/* @doc		Returns true if startDate is before endDate, false otherwise.
	 * 			Assumes the String parameters to have the format "YYYYMMDD".
	 * @author	Marco Trifance
	 * */
	public static boolean validateStringDates(String startDate, String endDate){
		// 1. Check if Empty
		if(startDate.isEmpty() || endDate.isEmpty())
			return false;
			
		Date sDate = getDateFromString(startDate);
		Date eDate = getDateFromString(endDate);
				
		// 2. Check if String can be parsed to date
		if(sDate == null || eDate == null)
			return false;
				
		return (!eDate.before(sDate));
	}
	
	/* @doc		Returns true if date is between startDate (inclusive) and endDate (inclusive). 
	 * 			Time information is ignored (only year, month and day are considered).
	 * @author  Marco Trifance
	 * */
	public static boolean isDateIncluded(Date date, Date startDate, Date endDate){
		
		// Get dates with 0 time information
		Date shortDate = getShortDate(date);
		Date shortStartDate = getShortDate(startDate);
		Date shortEndDate = getShortDate(endDate);
		
		// Compare dates
		if(shortDate.compareTo(shortStartDate) < 0 || shortDate.compareTo(shortEndDate) > 0){
			return false;
		}else{
			return true;
		}
	}
	
	/* @doc Returns a transformation of 'date' where time information
	 * 		is set to zero (hour, minute, second and millisecond).
	 * */
	private static Date getShortDate(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		return cal.getTime();
	}
	
	/*@doc 	  Returns true if date is between startDate (inclusive) and endDate (inclusive).
	 * 		  Time information is ignored (only year, month and day are considered).  
	 * 		  NOTE: 'startDate' and 'endDate' are provided in format "YYYYMMDD".
	 * @author Marco Trifance
	 * */
	public static boolean isDateIncluded(Date dateIn, String startDate, String endDate) {
		Date sd = getDateFromString(startDate);
		Date ed = getDateFromString(endDate);
		
		return isDateIncluded(dateIn, sd, ed);
	}
	
	/* @doc 	Returns the date representation of a string describing a date
	 * 			
	 * @author	Joakim Eliasson
	 * */
	public static Date getDateFromString(String dateString){
		
		DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		formatter.setLenient(false);		// *** Marco: allows to catch the parse-exception
		Date date;
		try {
			date = formatter.parse(dateString);
			return date;
		} catch (ParseException e) {
			//e.printStackTrace();
			return null;
		}
	}
}
