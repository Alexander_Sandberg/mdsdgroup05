/**
 */
package se.chalmers.cse.mdsd1617.group05;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IType Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getITypeManager()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ITypeManager extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true" ordered="false"
	 * @generated
	 */
	EList<Type> getTypes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" typeNameRequired="true" typeNameOrdered="false"
	 * @generated
	 */
	Type fetchTypeByName(String typeName);

} // ITypeManager
