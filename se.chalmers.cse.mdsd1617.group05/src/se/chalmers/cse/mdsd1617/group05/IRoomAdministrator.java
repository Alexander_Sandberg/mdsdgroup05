/**
 */
package se.chalmers.cse.mdsd1617.group05;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IRoom Administrator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getIRoomAdministrator()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IRoomAdministrator extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumRequired="true" roomNumOrdered="false" typeNameRequired="true" typeNameOrdered="false"
	 * @generated
	 */
	boolean changeRoomTypeOfRoom(int roomNum, String typeName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumRequired="true" roomNumOrdered="false" typeNameRequired="true" typeNameOrdered="false"
	 * @generated
	 */
	boolean addRoom(int roomNum, String typeName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumRequired="true" roomNumOrdered="false"
	 * @generated
	 */
	boolean removeRoom(int roomNum);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumRequired="true" roomNumOrdered="false"
	 * @generated
	 */
	boolean blockRoom(int roomNum);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumRequired="true" roomNumOrdered="false"
	 * @generated
	 */
	boolean unblockRoom(int roomNum);

} // IRoomAdministrator
