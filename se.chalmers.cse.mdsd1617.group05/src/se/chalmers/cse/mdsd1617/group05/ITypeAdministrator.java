/**
 */
package se.chalmers.cse.mdsd1617.group05;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IType Administrator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group05.Group05Package#getITypeAdministrator()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ITypeAdministrator extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" typeNameRequired="true" typeNameOrdered="false" priceRequired="true" priceOrdered="false" numOfBedsRequired="true" numOfBedsOrdered="false"
	 * @generated
	 */
	boolean addRoomType(String typeName, double price, int numOfBeds);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" oldNameRequired="true" oldNameOrdered="false" newNameRequired="true" newNameOrdered="false" newPriceRequired="true" newPriceOrdered="false" newNumOfBedsRequired="true" newNumOfBedsOrdered="false"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='updateRoomType()'"
	 * @generated
	 */
	boolean updateRoomType(String oldName, String newName, double newPrice, int newNumOfBeds);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" typeNameRequired="true" typeNameOrdered="false"
	 * @generated
	 */
	boolean removeRoomType(String typeName);

} // ITypeAdministrator
