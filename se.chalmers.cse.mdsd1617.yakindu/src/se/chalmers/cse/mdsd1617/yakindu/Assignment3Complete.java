package se.chalmers.cse.mdsd1617.yakindu;
import java.util.List;
import java.util.Map;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Assignment3Complete {
	private long currentBookingId = 50;
	private long currentReservationNumber = 0;

	private final int MAX_ROOMS = 2;
	
	//testingQ
	private ConcurrentLinkedQueue<Long> freeRooms = new ConcurrentLinkedQueue<Long>();
	private Map<Long, LinkedList<Long>> checkedInRooms = new ConcurrentHashMap<Long, LinkedList<Long>>();
	
	
	public void init(){
		for(int i = 0; i < MAX_ROOMS; i++){
			Room r = new Room(i);
			freeRooms.add(r.getNumber());
		}
	}
	
	//check if room with given id is free.
	public boolean checkRoom(long roomID){
		return Room.findRoomByNum(roomID).isFree();
	}
	
	public long takeFreeRoom(long bookingID, long resID){
		if(!freeRooms.isEmpty()){
			long roomNum = freeRooms.poll();
			Room.findRoomByNum(roomNum).setReservationID(resID);
			if(!checkedInRooms.containsKey(bookingID))
				checkedInRooms.put(Long.valueOf(bookingID), new LinkedList());
			checkedInRooms.get(bookingID).addLast(roomNum);
			return roomNum;
		}
		return  -1;
	}
	 
	
	public void returnOccupiedRoom(long bookingId, long roomId){
		if(!Room.isFree(roomId)){
			Room.findRoomByNum(roomId).setReservationID(-1);
			checkedInRooms.get(bookingId).remove(roomId);
			freeRooms.add(roomId);
		}
	}
	
	//Checkout problem
	public boolean isRoomBelongingToBooking(long bookingId, long room, long res0Book, long res0Room, long res1Book, long res1Room){
		return (bookingId == res0Book && res0Room == room) 
		|| (bookingId == res1Book && res1Room == room);
	}	

	public long getActiveBookingId(long bookSwitch, long book0Id, long book1Id){
		if(bookSwitch == 0){
			return book0Id;
		}else{
			return book1Id;
		}
	}
	
	
	//end of experimenting
	public long initiateBooking() {
		return currentBookingId++;
	}

	public boolean addRoomToBooking(long bookingId) {
		if (bookingId < 0 || bookingId > currentBookingId) {
			return false;
		} else if (currentReservationNumber >= MAX_ROOMS) {
			return false;
		} else {
			++currentReservationNumber;
			return true;
		}
	}
}
