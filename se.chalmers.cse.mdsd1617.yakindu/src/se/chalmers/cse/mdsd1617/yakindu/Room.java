package se.chalmers.cse.mdsd1617.yakindu;

import java.util.concurrent.ConcurrentHashMap;

class Room {
	private final long number;
	private long reservationID = -1;
	private static ConcurrentHashMap<Long, Room> rooms = new ConcurrentHashMap<Long, Room>();
	
	
	public Room(long id) {
		number = id;
		rooms.put(Long.valueOf(id),this);
	}
	
	public long getNumber(){
		return number;
	}
	
	public boolean isFree(){
		if (reservationID != -1)
			return false;
		return true;
	}
	
	public long getReservationID() {
		return reservationID;
	}
	
	public void setReservationID(long resID){
		reservationID = resID;
	}
	
	public static Room findRoomByNum(long roomId){
		return rooms.get(Long.valueOf(roomId));
	}
	
	public static boolean isFree(long roomId){
		return rooms.get(Long.valueOf(roomId)).isFree();
	}
	
}